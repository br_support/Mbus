
ACTION SharkyRawDataProcess: 
	////////////////////// Processing of responce to User Data Readout request for Sharky 775 ///////////////////////
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[24];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[25];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[26];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[27];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentEnergy1 := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[31];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[32];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[33];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[34];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrTarifReg1Energy := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[38];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[39];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[40];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[41];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrTarifReg2Volume := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[44];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[45];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[46];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[47];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentVolume1 := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[50];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[51];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[52];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[53];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentPower := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[56];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[57];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[58];
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 3;
	decodeBCD();
	SharkyResponce.CurrentFlowRate := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[61];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[62];
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 2;
	decodeBCD();
	SharkyResponce.CurrentForwardTemp := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[65];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[66];
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 2;
	decodeBCD();
	SharkyResponce.CurrentReturnTemp := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[69];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[70];
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 2;
	decodeBCD();
	SharkyResponce.CurrentDifferenceTemp := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[73];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[74];
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 2;
	decodeBCD();
	SharkyResponce.OperatingDays := decodeBCD.OutputVal;
	//vyresit pripad length == 1
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[77];
	decodeBCD.InputData[1] := 0;
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 1;
	decodeBCD();
	SharkyResponce.CurrentTime.Minute := DINT_TO_USINT(decodeBCD.OutputVal);
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[78];
	decodeBCD.InputData[1] := 0;
	decodeBCD.InputData[2] := 0;
	decodeBCD.InputData[3] := 0;
	decodeBCD.Length := 1;
	decodeBCD();
	SharkyResponce.CurrentTime.Hour := DINT_TO_USINT(decodeBCD.OutputVal);
	
	decodeDate.ReadVal := SplitMBusResponse_0.FrameCopy[79];
	decodeDate.ReadVal := decodeDate.ReadVal + SplitMBusResponse_0.FrameCopy[80]*256;
	decodeDate;
	SharkyResponce.CurrentTime.Date := decodeDate.Output;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[83];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[84];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[85];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[86];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.AccountingDate.Energy := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[89];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[90];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[91];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[92];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.AccountingDate.Volume := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[96];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[97];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[98];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[99];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.AccountingDate.Tariff_1 := decodeBCD.OutputVal;
	
	decodeDate.ReadVal := SplitMBusResponse_0.FrameCopy[109];
	decodeDate.ReadVal := decodeDate.ReadVal + SplitMBusResponse_0.FrameCopy[110]*256;
	decodeDate;
	SharkyResponce.AccountingDate.Date := decodeDate.Output;
	
	decodeDate.ReadVal := SplitMBusResponse_0.FrameCopy[114];
	decodeDate.ReadVal := decodeDate.ReadVal + SplitMBusResponse_0.FrameCopy[115]*256;
	decodeDate;
	SharkyResponce.AccountingDate.NextAccountingDate := decodeDate.Output;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[119];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[120];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[121];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[122];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentEnergy2 := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[126];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[127];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[128];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[129];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentVolume2 := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[133];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[134];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[135];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[136];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentEnergy3 := decodeBCD.OutputVal;
	
	decodeBCD.InputData[0] := SplitMBusResponse_0.FrameCopy[140];
	decodeBCD.InputData[1] := SplitMBusResponse_0.FrameCopy[141];
	decodeBCD.InputData[2] := SplitMBusResponse_0.FrameCopy[142];
	decodeBCD.InputData[3] := SplitMBusResponse_0.FrameCopy[143];
	decodeBCD.Length := 4;
	decodeBCD();
	SharkyResponce.CurrentVolume3 := decodeBCD.OutputVal;
	
	decodeDate.ReadVal := SplitMBusResponse_0.FrameCopy[147];
	decodeDate.ReadVal := decodeDate.ReadVal + SplitMBusResponse_0.FrameCopy[148]*256;
	decodeDate;
	SharkyResponce.LastAccountingDate := decodeDate.Output;
	
	decodeDate.ReadVal := SplitMBusResponse_0.FrameCopy[153];
	decodeDate.ReadVal := decodeDate.ReadVal + SplitMBusResponse_0.FrameCopy[154]*256;
	decodeDate;
	SharkyResponce.NextAccountingDate := decodeDate.Output;
END_ACTION

ACTION ApatorDataProcess:
	Date := UDINT_TO_UINT(ROR((Apator_0.Time.ReadValue AND 16#FF_FF_00_00), 16));
	Time := UDINT_TO_UINT(Apator_0.Time.ReadValue AND 16#00_00_FF_FF);
	
	Apator_0.Time.Day 	:= UINT_TO_USINT(Date AND 16#00_1F);
	Apator_0.Time.Month := UINT_TO_USINT(ROR((Date AND 16#0F_00), 8));
	Apator_0.Time.Year 	:= UINT_TO_USINT(ROR((ROR((Date AND 16#F0_00), 4) OR (Date AND 16#00_E0)), 5));

	Apator_0.Time.Minute	:=	UINT_TO_USINT(Time AND 16#00_FF);
	Apator_0.Time.Hour		:=	UINT_TO_USINT(ROR((Time AND 16#FF_00),8))-31;
	
	Date := Apator_0.LoggedVolume.Timestamp.ReadValue;
	
	Apator_0.LoggedVolume.Timestamp.Day 	:= UINT_TO_USINT(Date AND 16#00_1F);
	Apator_0.LoggedVolume.Timestamp.Month	:= UINT_TO_USINT(ROR((Date AND 16#0F_00), 8));
	Apator_0.LoggedVolume.Timestamp.Year 	:= UINT_TO_USINT(ROR((ROR((Date AND 16#F0_00), 4) OR (Date AND 16#00_E0)), 5));
END_ACTION
