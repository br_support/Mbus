
(* TODO: Add your comment here *)
FUNCTION_BLOCK BCD_dec
	OutputVal := 0;
	index := 0;
	REPEAT
		OutputVal := OutputVal + (InputData[index] AND 16#0F) * REAL_TO_DINT((EXPT(10, index*2)));
		OutputVal := OutputVal + ROR((InputData[index] AND 16#F0),4) * REAL_TO_DINT((EXPT(10, 1+(index*2))));
		index := index + 1;
	UNTIL
		index = Length
	END_REPEAT;
	

END_FUNCTION_BLOCK
