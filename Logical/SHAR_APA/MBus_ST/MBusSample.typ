(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusSample_C
 * Datei: MBusSample_C.typ
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Lokale Datentypen des Programs MBusSample_C
 ********************************************************************)

TYPE
	SharkyTelegram : 	STRUCT 
		CurrTarifReg1Energy : DINT;
		CurrTarifReg2Volume : DINT;
		CurrentEnergy3 : DINT;
		CurrentEnergy2 : DINT;
		CurrentEnergy1 : DINT;
		CurrentVolume2 : DINT;
		CurrentVolume3 : DINT;
		NextAccountingDate : DateMBus;
		LastAccountingDate : DateMBus;
		CurrentVolume1 : DINT;
		CurrentFlowRate : DINT;
		CurrentPower : DINT;
		CurrentForwardTemp : DINT;
		CurrentReturnTemp : DINT;
		CurrentDifferenceTemp : DINT;
		CurrentTime : Date_N_Time;
		OperatingDays : DINT;
		AccountingDate : type_AccountingDate;
	END_STRUCT;
	type_AccountingDate : 	STRUCT 
		Date : DateMBus;
		Volume : DINT;
		Energy : DINT;
		Tariff_2 : DINT;
		Tariff_1 : DINT;
		NextAccountingDate : DateMBus;
	END_STRUCT;
	Date_N_Time : 	STRUCT 
		Minute : USINT;
		Hour : USINT;
		Date : DateMBus;
	END_STRUCT;
	DateMBus : 	STRUCT 
		Year : USINT;
		Month : USINT;
		Day : USINT;
	END_STRUCT;
	MBusDate : 	STRUCT 
		Year : USINT;
		Month : USINT;
		ReadValue : UINT;
		Day : USINT;
	END_STRUCT;
	Val_TimeStamp : 	STRUCT 
		LoggedValue : UDINT;
		Timestamp : MBusDate;
	END_STRUCT;
	MBusApator : 	STRUCT 
		LoggedVolume : Val_TimeStamp;
		SerialNumber : UDINT;
		CurrentFlowRate : INT;
		CorrentVolume : DINT;
		OperationTime : UDINT;
		Time : DeviceTime;
		ApatorErr : UDINT;
	END_STRUCT;
	DeviceTime : 	STRUCT 
		Year : USINT;
		Month : USINT;
		Minute : USINT;
		Hour : USINT;
		Day : USINT;
		ReadValue : UDINT;
	END_STRUCT;
END_TYPE
