﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.3.3.196?>
<Program Version="1.00.1" xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Lokale Variablen" Private="true">UserInterface.var</File>
    <File Description="Lokale Variablen" Private="true">MBusSample.var</File>
    <File Description="Lokale Datentypen" Private="true">MBusSample.typ</File>
    <File Description="Cyclic code">MBus_STCyclic.st</File>
    <File Description="Initialization code">MBus_STInit.st</File>
    <File>UserConfiguration.st</File>
    <File Description="Split the Incoming Data of Slave 1">UserInterfaceSlave.st</File>
  </Files>
</Program>