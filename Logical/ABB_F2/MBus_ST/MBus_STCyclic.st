(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MBus_ST
 * File: MBus_STCyclic.st
 * Author: peterlechnes
 * Created: April 01, 2015
 ********************************************************************
 * Implementation of program MBus_ST
 ********************************************************************)

PROGRAM _CYCLIC

	(* 1: Handle the X20CS1012 FlatGen Interface ----------------------------------------------------------------- *)
	
		//FltGenHandler_0.output_data := CreateMBusRequest_0.output_data;
		FltGenHandler_0.bExecuteRequest := MBusSlaveManager_0.SlaveManPaketIf.bSendRequest;
		memcpy(ADR(FltGenHandler_0.output_data), ADR(CreateMBusRequest_0.output_data), SIZEOF(FltGenHandler_0.output_data));
		FltGenHandler_0();
	
	
	(* 2:  Create the MBus Paket  -------------------------------------------------------------------------------- *)
		CreateMBusRequest_0.Requestpar.PaketCounter 		:= FltGenHandler_0.PaketCounter;
		CreateMBusRequest_0.Requestpar.SlaveSeriennummer	:= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.SecundarAdr;
		CreateMBusRequest_0.Requestpar.PrimarAdress 		:= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.PrimarAdr;
		CreateMBusRequest_0.Requestpar.Adressierungsart 	:= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.addressingTyp;  

		CreateMBusRequest_0.Requestpar.Baudrate 			:= BAUDRATE_ISTA; 
		CreateMBusRequest_0();
	
	
	(* 3: Split the MBus Response TO an User Struct ---------------------------------------------------------------*)
		SplitMBusResponse_0.bFrameRecive := FltGenHandler_0.bFrameRecive;
	
		r_trig_0.CLK 					 := FltGenHandler_0.bFrameRecive;
		r_trig_0();
	
		memcpy(ADR(SplitMBusResponse_0.FrameCopy[0]), ADR(FltGenHandler_0.FrameCopy[0]), SIZEOF(SplitMBusResponse_0.FrameCopy));
		SplitMBusResponse_0();
	
	(* Schnittstelle zum Userinterface *)
		IF (r_trig_0.Q) THEN ResponsePaketAnalyser;  END_IF;
	
		FltGenHandler_0.bFrameEvaluated := SplitMBusResponse_0.bFrameEvaluated;

	(* 4: SlaveManager Organisation between the Slaves. ------------------------------------------------------------*)
		MBusSlaveManager_0.SlaveManagerIf.bEnable 		   := bStart;
		MBusSlaveManager_0.SlaveManagerIf.bFrameEvaluated  := SplitMBusResponse_0.bFrameEvaluated;
		MBusSlaveManager_0();
	
	(* -------------------------------------------------------------------------------------------------------------*)


END_PROGRAM
