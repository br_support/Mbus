/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusSample_C
 * Datei: MBusSample_CInit.c
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Implementierung des Programms MBusSample_C
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT MBusSample_CInit(void)
{
	
	SlaveHandlerConfiguration();

}
