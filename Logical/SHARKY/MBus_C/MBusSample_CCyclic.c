/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusSample_C
 * Datei: MBusSample_CCyclic.c
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Implementierung des Programms MBusSample_C
 ********************************************************************/

/* ------------------------------ History ---------------------------*/

/* ------------------------------------------------------------------*/

/* ------------------------------Contens-----------------------------*/

/* ------------------------------------------------------------------*/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif



void _CYCLIC MBusSample_CCyclic( void )
{

	/* 1: Handle the X20CS1012 FlatGen Interface ----------------------------------------------------------------- */
	//FltGenHandler_0.output_data = CreateMBusRequest_0.output_data;
	FltGenHandler_0.bExecuteRequest = MBusSlaveManager_0.SlaveManPaketIf.bSendRequest;
	memcpy(&FltGenHandler_0.output_data, &CreateMBusRequest_0.output_data, sizeof(FltGenHandler_0.output_data));
	FltGenHandler(&FltGenHandler_0);
	
	
	/* 2:  Create the MBus Paket  -------------------------------------------------------------------------------- */
	CreateMBusRequest_0.Requestpar.PaketCounter 		= FltGenHandler_0.PaketCounter;
	CreateMBusRequest_0.Requestpar.SlaveSeriennummer	= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.SecundarAdr;
	CreateMBusRequest_0.Requestpar.PrimarAdress 		= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.PrimarAdr;
	CreateMBusRequest_0.Requestpar.Adressierungsart 	= MBusSlaveManager_0.SlaveManPaketIf.PaketIf.addressingTyp;  

	CreateMBusRequest_0.Requestpar.Baudrate 			= BAUDRATE_ISTA; 
	CreateMBusRequest(&CreateMBusRequest_0);
	
	
	/* 3: Split the MBus Response to an User Struct ---------------------------------------------------------------*/
	SplitMBusResponse_0.bFrameRecive = FltGenHandler_0.bFrameRecive;
	r_trig_0.CLK = FltGenHandler_0.bFrameRecive;
	R_TRIG(&r_trig_0);
	
	memcpy(&SplitMBusResponse_0.FrameCopy[0], &FltGenHandler_0.FrameCopy[0], sizeof(SplitMBusResponse_0.FrameCopy));
	
	SplitMBusResponse(&SplitMBusResponse_0);
	if (r_trig_0.Q)
	{
		Paketauswertung ();   /* Schnittstelle zum Userinterface */
	}
	
	FltGenHandler_0.bFrameEvaluated = SplitMBusResponse_0.bFrameEvaluated;
	
	/* 4: SlaveManager Organisation between the Slaves. ------------------------------------------------------------*/
	MBusSlaveManager_0.SlaveManagerIf.bEnable 		   = bStart;
	MBusSlaveManager_0.SlaveManagerIf.bFrameEvaluated = SplitMBusResponse_0.bFrameEvaluated;
	MBusSlaveManager_0;
	
	MBusSlaveManager(&MBusSlaveManager_0);
	
	/* -------------------------------------------------------------------------------------------------------------*/
		
}
