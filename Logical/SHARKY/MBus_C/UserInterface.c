/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusSample_C
 * Datei: MBusSample_CInit.c
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Implementierung des Programms MBusSample_C
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void SlaveHandlerConfiguration(void);
void Paketauswertung (void); 

void SplitSlave1_Resp(void);
void SplitSlave2_Resp(void);
void SplitSlave3_Resp(void);
void SplitSlave4_Resp(void);
void SplitSlave5_Resp(void);

void SlaveHandlerConfiguration(void)
{
	MBusSlaveManager_0.SlaveManagerIf.num_of_Slave					= 2; 
	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].addressingTyp 		= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].PrimarAdr	 		= PIMAER_ADRESS_BROADCAST;
	//MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].SecundarAdr 		= 0xFF00FF00; 	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].SendCycleTime 		= 15000; 
	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].addressingTyp 		= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].PrimarAdr	 		= 1;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].SecundarAdr		= 0xFF00FF00; 
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].SendCycleTime 		= 15000; 
	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].addressingTyp 		= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].PrimarAdr	 		= 2;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].SecundarAdr		= 0xFF00FF00;  
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].SendCycleTime 		= 15000; 
}

void Paketauswertung()
{
	
	switch (MBusSlaveManager_0.SlaveManPaketIf.aktSlaveNr)
	{
		case 1:
			SplitSlave1_Resp();
			break;
	
		case 2:
			SplitSlave2_Resp();
			break;
		
		case 3:
			SplitSlave3_Resp();
			break;
		
		case 4:
			SplitSlave4_Resp();
			break;
		
		case 5:
			SplitSlave5_Resp();
			break;
		
		default:
	
			break;
	}
	
}

void SplitSlave1_Resp()
{
	if (FltGenHandler_0.FrameCopy[2] == 0x70 || FltGenHandler_0.FrameCopy[2] == 0x0 || FltGenHandler_0.FrameCopy[2] == 0x50 || FltGenHandler_0.FrameCopy[2] == 0x30) 
	{
		
		ABB_F4_Energy.Header.PaketZahler 	=  FltGenHandler_0.FrameCopy[1];
		ABB_F4_Energy.Header.ErrorNummer  	=  FltGenHandler_0.FrameCopy[2];
		ABB_F4_Energy.Header.StartSequenz1  =  FltGenHandler_0.FrameCopy[3];
		ABB_F4_Energy.Header.L_Feld_1  		=  FltGenHandler_0.FrameCopy[4];
		ABB_F4_Energy.Header.L_Feld_2  		=  FltGenHandler_0.FrameCopy[5];
		ABB_F4_Energy.Header.StartSequenz2  =  FltGenHandler_0.FrameCopy[6];
		ABB_F4_Energy.Header.C_Feld			=  FltGenHandler_0.FrameCopy[7];
		
		if (ABB_F4_Energy.Header.C_Feld == RSP_UD)
		{
			//strcpy(&strProtokollTyp = 'LangSteuersatz');
		}
		
		ABB_F4_Energy.Header.A_Feld_PrimarAdr	=  FltGenHandler_0.FrameCopy[8];
		ABB_F4_Energy.Header.CI_Feld			=  FltGenHandler_0.FrameCopy[9]; // Wird im Kapitel 2.8 behandelt. 
		
		if (ABB_F4_Energy.Header.CI_Feld == 0x72)
		{
			// Modus 1
		}
		else if (ABB_F4_Energy.Header.CI_Feld == 0x76)
		{
			// Modus 2 Grundlagen zum M Bus => Kapitel 2.8.2
		}
				
		memcpy(&ABB_F4_Energy.AnwenderDaten.Identifikationsnummer, &FltGenHandler_0.FrameCopy[10], sizeof(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer));
				
		//Seriennummer =  DWordBCDtoDezimal(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer);
		//ABB_F4_Energy.AnwenderSchnittstelle.Seriennummer =  DWordBCDtoDezimal(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer);
		ABB_F4_Energy.AnwenderSchnittstelle.Seriennummer =  ABB_F4_Energy.AnwenderDaten.Identifikationsnummer;
	}
}

void SplitSlave2_Resp()
{
	if (FltGenHandler_0.FrameCopy[2] == 0x70 || FltGenHandler_0.FrameCopy[2] == 0x0 || FltGenHandler_0.FrameCopy[2] == 0x50 || FltGenHandler_0.FrameCopy[2] == 0x30) 
	{
		IstaSlave.Header.PaketZahler 	=  FltGenHandler_0.FrameCopy[1];
		
		memcpy((&IstaSlave.AnwenderDaten.Identifikationsnummer), &FltGenHandler_0.FrameCopy[10], sizeof(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer));
	}
	
}

void SplitSlave3_Resp()
{

}

void SplitSlave4_Resp()
{

}

void SplitSlave5_Resp()
{

}