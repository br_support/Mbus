(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusSample_C
 * Datei: MBusSample_C.typ
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Lokale Datentypen des Programs MBusSample_C
 ********************************************************************)

TYPE
	SharkyTelegram : 	STRUCT 
		CurrTarifReg1Energy : DINT; (*GJ*)
		CurrTarifReg2Volume : DINT; (*GJ*)
		CurrentEnergy3 : DINT; (*GJ*)
		CurrentEnergy2 : DINT; (*GJ*)
		CurrentEnergy1 : DINT; (*GJ*)
		CurrentVolume2 : DINT; (*10l*)
		CurrentVolume3 : DINT; (*10l*)
		NextAccountingDate : DateM;
		LastAccountingDate : DateM;
		CurrentVolume1 : DINT; (*10l*)
		CurrentFlowRate : DINT; (*0.001 m^3/h*)
		CurrentPower : DINT; (*W*)
		CurrentForwardTemp : DINT; (*�C*)
		CurrentReturnTemp : DINT; (*�C*)
		CurrentDifferenceTemp : DINT; (*�C*)
		CurrentTime : Date_N_Time;
		OperatingDays : DINT; (*days*)
		AccountingDate : type_AccountingDate;
	END_STRUCT;
	type_AccountingDate : 	STRUCT 
		Date : DateM;
		Volume : DINT;
		Energy : DINT;
		Tariff_2 : DINT;
		Tariff_1 : DINT;
		NextAccountingDate : DateM;
	END_STRUCT;
	Date_N_Time : 	STRUCT 
		Minute : USINT;
		Hour : USINT;
		Date : DateM;
	END_STRUCT;
	DateM : 	STRUCT 
		Year : USINT;
		Month : USINT;
		Day : USINT;
	END_STRUCT;
END_TYPE
