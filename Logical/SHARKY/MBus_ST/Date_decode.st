
(* TODO: Add your comment here *)
FUNCTION_BLOCK Date_decode
	Output.Day 	 := UINT_TO_USINT(ReadVal AND 16#00_1F);
	Output.Month := UINT_TO_USINT(ROR((ReadVal AND 16#0F_00), 8));
	Output.Year  := UINT_TO_USINT(ROR((ROR((ReadVal AND 16#F0_00), 4) OR (ReadVal AND 16#00_E0)), 5));
END_FUNCTION_BLOCK
