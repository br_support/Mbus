(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MBus_ST
 * File: SlaveHandlerConfiguration.st
 * Author: peterlechnes
 * Created: April 02, 2015
 ********************************************************************
 * Implementation of program MBus_ST
 ********************************************************************) 

(* Configuration of the Slave Handler *)
ACTION SlaveHandlerConfiguration: 
	MBusSlaveManager_0.SlaveManagerIf.num_of_Slave					:= 1; 
	
	//MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].addressingTyp 		:= SEKUNDAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].addressingTyp 		:= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].PrimarAdr	 		:= 1;//PIMAER_ADRESS_BROADCAST;
	//MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].SecundarAdr 		:= 16#FF00FF00; 	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[0].SendCycleTime 		:= T#3s;//T#15000ms; 
	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].addressingTyp 		:= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].PrimarAdr	 		:= 1;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].SecundarAdr		:= 16#FF00FF00; 
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[1].SendCycleTime 		:= T#3s;//T#15000ms;
	
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].addressingTyp 		:= PRIMAR_ADR;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].PrimarAdr	 		:= 1;
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].SecundarAdr		:= 16#FF00FF00;  
	MBusSlaveManager_0.SlaveManagerIf.SlaveIf[2].SendCycleTime 		:= T#15000ms;
END_ACTION
