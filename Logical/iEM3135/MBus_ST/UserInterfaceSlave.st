(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MBus_ST
 * File: SlaveResp1.st
 * Author: peterlechnes
 * Created: April 02, 2015
 ********************************************************************
 * Implementation of program MBus_ST
 ********************************************************************) 

ACTION ResponsePaketAnalyser :

	CASE MBusSlaveManager_0.SlaveManPaketIf.aktSlaveNr OF
		1: SplitSlave1_Resp;
		2: SplitSlave2_Resp;
		3: SplitSlave3_Resp;
		4: SplitSlave4_Resp;
		5: SplitSlave5_Resp;	
	END_CASE;

	END_ACTION

(* Split the incoming Data of Slave 1 *)
ACTION SplitSlave1_Resp :
	IF (FltGenHandler_0.FrameCopy[2] = 16#70 OR FltGenHandler_0.FrameCopy[2] = 16#0 OR FltGenHandler_0.FrameCopy[2] = 16#50 OR FltGenHandler_0.FrameCopy[2] = 16#10) THEN
		
		ABB_F4_Energy.Header.PaketZahler 	:=  FltGenHandler_0.FrameCopy[1];
		ABB_F4_Energy.Header.ErrorNummer  	:=  FltGenHandler_0.FrameCopy[2];
		ABB_F4_Energy.Header.StartSequenz1  :=  FltGenHandler_0.FrameCopy[3];
		ABB_F4_Energy.Header.L_Feld_1  		:=  FltGenHandler_0.FrameCopy[4];
		ABB_F4_Energy.Header.L_Feld_2  		:=  FltGenHandler_0.FrameCopy[5];
		ABB_F4_Energy.Header.StartSequenz2  :=  FltGenHandler_0.FrameCopy[6];
		ABB_F4_Energy.Header.C_Feld			:=  FltGenHandler_0.FrameCopy[7];
		
		IF (ABB_F4_Energy.Header.C_Feld = RSP_UD) THEN
			//strcpy(ADRstrProtokollTyp := 'LangSteuersatz');
		END_IF;
		
		ABB_F4_Energy.Header.A_Feld_PrimarAdr	:=  FltGenHandler_0.FrameCopy[8];
		ABB_F4_Energy.Header.CI_Feld			:=  FltGenHandler_0.FrameCopy[9]; // Wird im Kapitel 2.8 behandelt. 
		
		IF (ABB_F4_Energy.Header.CI_Feld = 16#72) THEN
			// Modus 1
		ELSIF (ABB_F4_Energy.Header.CI_Feld = 16#76) THEN
			// Modus 2 Grundlagen zum M Bus :=> Kapitel 2.8.2
		END_IF;
				
		memcpy(ADR(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer), ADR(FltGenHandler_0.FrameCopy[10]), SIZEOF(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer));
				
		//Seriennummer :=  DWordBCDtoDezimal(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer);
		//ABB_F4_Energy.AnwenderSchnittstelle.Seriennummer :=  DWordBCDtoDezimal(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer);
		ABB_F4_Energy.AnwenderSchnittstelle.Seriennummer :=  ABB_F4_Energy.AnwenderDaten.Identifikationsnummer;
		END_IF;
	
		END_ACTION


ACTION SplitSlave2_Resp :

	IF (FltGenHandler_0.FrameCopy[2] = 16#70 OR FltGenHandler_0.FrameCopy[2] = 16#0 OR FltGenHandler_0.FrameCopy[2] = 16#50 OR FltGenHandler_0.FrameCopy[2] = 16#30) THEN
		IstaSlave.Header.PaketZahler 	:=  FltGenHandler_0.FrameCopy[1];

		memcpy(ADR(IstaSlave.AnwenderDaten.Identifikationsnummer), ADR(FltGenHandler_0.FrameCopy[10]), SIZEOF(ABB_F4_Energy.AnwenderDaten.Identifikationsnummer));
	END_IF;

END_ACTION 
				
ACTION SplitSlave3_Resp :
END_ACTION

ACTION SplitSlave4_Resp :
END_ACTION

				
ACTION SplitSlave5_Resp :
END_ACTION