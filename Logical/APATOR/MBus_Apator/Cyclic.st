
PROGRAM _CYCLIC
	(* Insert code here *)
	Date := UDINT_TO_UINT(ROR((Apator_0.Time.ReadValue AND 16#FF_FF_00_00), 16));
	Time := UDINT_TO_UINT(Apator_0.Time.ReadValue AND 16#00_00_FF_FF);
	
	Apator_0.Time.Day 	:= UINT_TO_USINT(Date AND 16#00_1F);
	Apator_0.Time.Month := UINT_TO_USINT(ROR((Date AND 16#0F_00), 8));
	Apator_0.Time.Year 	:= UINT_TO_USINT(ROR((ROR((Date AND 16#F0_00), 4) OR (Date AND 16#00_E0)), 5));

	Apator_0.Time.Minute	:=	UINT_TO_USINT(Time AND 16#00_FF);
	Apator_0.Time.Hour		:=	UINT_TO_USINT(ROR((Time AND 16#FF_00),8))-31;
	
	Date := Apator_0.LoggedVolume.Timestamp.ReadValue;
	
	Apator_0.LoggedVolume.Timestamp.Day 	:= UINT_TO_USINT(Date AND 16#00_1F);
	Apator_0.LoggedVolume.Timestamp.Month	:= UINT_TO_USINT(ROR((Date AND 16#0F_00), 8));
	Apator_0.LoggedVolume.Timestamp.Year 	:= UINT_TO_USINT(ROR((ROR((Date AND 16#F0_00), 4) OR (Date AND 16#00_E0)), 5));
	
END_PROGRAM
