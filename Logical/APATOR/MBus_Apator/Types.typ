
TYPE
	MBusDate : 	STRUCT 
		Year : USINT;
		Month : USINT;
		ReadValue : UINT;
		Day : USINT;
	END_STRUCT;
	Val_TimeStamp : 	STRUCT 
		LoggedValue : UDINT;
		Timestamp : MBusDate;
	END_STRUCT;
	MBusApator : 	STRUCT 
		LoggedVolume : Val_TimeStamp;
		SerialNumber : UDINT;
		CurrentFlowRate : INT; (**10^(-3) [(m^3)/(h)]*)
		CorrentVolume : DINT; (**10^(-3) [m^3]*)
		OperationTime : UDINT; (*[Days]*)
		Time : DeviceTime;
		ApatorErr : UDINT;
	END_STRUCT;
	DeviceTime : 	STRUCT 
		Year : USINT;
		Month : USINT;
		Minute : USINT;
		Hour : USINT;
		Day : USINT;
		ReadValue : UDINT;
	END_STRUCT;
END_TYPE
