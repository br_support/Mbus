(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * Programm: cc
 * Datei: cc.typ
 * Autor: admin
 * Erstellt: 4. Oktober 2012
 ********************************************************************
 * Lokale Datentypen des Programs cc
 ********************************************************************)

TYPE
	FB_if_par_typ : 	STRUCT 
		Adressierungsart : USINT;
		PaketCounter : USINT;
		PrimarAdress : USINT;
		SlaveSeriennummer : UDINT;
		Baudrate : UINT;
	END_STRUCT;
END_TYPE

(*1: MBusPacket*)

TYPE
	MBusAnfrage_typ : 	STRUCT 
		Hauptteil : Header_typ;
		IndexRecord0 : IndexRecord0_typ;
		IndexRecord1 : IndexRecord1_typ;
	END_STRUCT;
END_TYPE

(*1.1 Haptteil*)

TYPE
	Header_typ : 	STRUCT 
		PacketZahler : USINT := 1; (*Um das Antworttelegramm einer Anfrage zuordnen zu k�nnen*)
		IndexRecordCount : USINT; (*Anzahl der folgenden Records*)
		ProtokollTyp1 : USINT; (*ProtokollTyp  Reserviert*)
		ProtokollTyp2 : USINT; (*ProtokollTyp  Reserviert*)
	END_STRUCT;
END_TYPE

(*1.2 IndexRecord0*)

TYPE
	Rec0Einleitung_typ : 	STRUCT 
		IndexRecordType : USINT;
		Counter : USINT;
		LenParBlockLow : USINT;
		LenParBlockHigh : USINT;
	END_STRUCT;
	Rec0KonfigParAdrArt_typ : 	STRUCT 
		Parameternummer : USINT;
		Lange : USINT;
		Adressierungsart : USINT;
	END_STRUCT;
	Rec0KonfigParAdresse_typ : 	STRUCT 
		Parameternummer : USINT;
		Lange : USINT;
		Adresse : ARRAY[0..3]OF USINT;
	END_STRUCT;
	Rec0KonfigParBaudrate_typ : 	STRUCT 
		Parameternummer : USINT;
		Lange : USINT;
		Baudrate : UINT;
		BaudrateLow : USINT;
		BaudrateHige : USINT;
	END_STRUCT;
	Rec0KonfigParTimeOutOffset_typ : 	STRUCT 
		Parameternummer : USINT;
		Lange : USINT;
		TimeoutOffset : USINT;
	END_STRUCT;
	Rec0KonfigParZusatzframe_typ : 	STRUCT 
		Parameternummer : USINT;
		Lange : USINT;
		Zusatzframe : USINT;
	END_STRUCT;
	IndexRecord0_typ : 	STRUCT 
		Einleitung : Rec0Einleitung_typ;
		KonfigParAdrArt : Rec0KonfigParAdrArt_typ;
		KonfigParAdresse : Rec0KonfigParAdresse_typ;
		KonfigParBaudrate : Rec0KonfigParBaudrate_typ;
		KonfigParTimeOutOffset : Rec0KonfigParTimeOutOffset_typ;
		KonfigParZusatzframe : Rec0KonfigParZusatzframe_typ;
	END_STRUCT;
END_TYPE

(*IndexRecord1_typ*)

TYPE
	Rec1Einleitung_typ : 	STRUCT 
		IndexRecordType : USINT;
		Counter : USINT;
		LangeParameterdatenLow : USINT;
		LangeParameterdatenHigh : USINT;
	END_STRUCT;
	Rec1DataParameter_typ : 	STRUCT 
		ParameterNumber : USINT;
		DataIndex : USINT;
	END_STRUCT;
	IndexRecord1_typ : 	STRUCT 
		Einleitung : Rec1Einleitung_typ;
		DataParameter : ARRAY[0..1]OF Rec1DataParameter_typ;
	END_STRUCT;
END_TYPE
