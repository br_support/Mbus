/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: MBusSlaveManager.c
 * Autor: peterlechnes
 * Erstellt: 5. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/
/********************************************************************
* Bernecker & Rainer
	********************************************************************
	* Programm: M Bus Handling
	* Datei:  M Bus Handling
	* Autor: Stefan Peterlechner
	* Erstellt: 4. Oktober 2012
 ********************************************************************
* Implementierung des Programms cc
	********************************************************************/
/*  ----------------------- History --------------------------------*/
/*  1.00.1 4.10.2012 First Sample									*/
/*  1.00.2 6.10.2012 implement the Error Handling 					*/
/*  1.00.3 15.10.2012 change the Funktion fltReadCall				*/
/*  1.00.4 16.10.2012 translate Automation Basic Sample to          */
/*                    Struktur Text 								*/
/*  1.01.0 17.10.2012 Add BCD Funktions. 							*/
/* 					  Add Parameter Response Struktur				*/
/*  1.02.0 05.11.2012 												*/
/*					�berfl�ssige Variablen s�ubern					*/
/*					Beim Packet zusammenstellen eine Erweiterung	*/
/*  1.03.0 18.12.2012 �nderung im AsFltGen Ablauf					*/
/*  1.03.1 11.03.2012 �nderung in der Auswertung des Parameter Resp	*/
/*  1.03.2 11.03.2012 Peterlechnes: Erweiterung f�r ABB Slaves		*/
/*  1.03.3 02.01.2013 Peterlechnes: Erweiterung Slavemanager 		*/
/* Konfig Action + getrennte Betrachtung von Slaves. Jeden Slave f�r*/
/* sich betrachten erh�lt eine eigene Fehlerauswertung, Bis zur 	*/
/* Version 1.03.2 gab es am Bus eine gemeinsame Fehlermeldung.		*/
/* 1.0.3.4 02.05.2013 Peterlechnes: Erweiterung der Fehlernummern	*/
/* in der Auswerte Action: "MbusPacketAuswerten".					*/
/* 1.0.3.5 02.05.2013 Peterlechnes: swap von Energie + 'Volumen		*/
/* 1.0.4.0 07.11.2019 Peterlechnes: Behebung Pagefault im Kompiler 6.11 */
/* 																	*/

/* ++++++++++++++++++++++ Inhaltsverzeichnis  +++++++++++++++++++++ */
/* 1: AS Flat Gen Bedienung    					  					*/
/* 2 Build the M Bus Packet. 										*/
/* 3: Copy to Applikation Frame Auswertung / Den Frame Auswerten 	*/ 
/* und auf die entsprechenden Applikationsdatenpunkte kopieren. 	*/
/* 4: Slave Handler umschaltung zwischen den Slaves / Adressenvergabe  					*/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Organisation Sequence between the Slaves */
void MBusSlaveManager(struct MBusSlaveManager* inst)
{
	USINT index; 
	/* Diese Action Konfiguriert die Slave Verwaltung:  Setzen von default Werten. 	*/
	/* 1: Salve Anzahl, Wieviele Busteilenhmer h�ngen am Master						*/			
	//	inst->SlaveManager. 			= NUMBER_OF_SLAVES; // Number_of_Slave;
	
	//	 2: Auswahl der Adressierungsart. Soll Sekund�r = die Seriennummer ist die   		
	//	 Slaveadresse oder Prim�r - Eine am Slave Eingestellte Knoten oder Display 		
	//	 Nummer zwischen 1 und 255 ist die Slaveadresse  254 w�re die Broad Cast Adresse 	

	//	MBusAnfrage.IndexRecord0.KonfigParAdrArt.Adressierungsart = PRIMAR_ADR;
	//MBusAnfrage.IndexRecord0.KonfigParAdrArt.Adressierungsart = SEKUNDAR_ADR;

	/*  3: Konfiguration der Slave Adressen		*/		
	
//	for (index = 0; index < MAX_NUM_OF_SLAVE; index++)
//	{
//		inst->SlaveManager.Slave[0].SlaveIf = inst->SlaveManagerIf;
//	}
	
	//	inst->SlaveManager.Slave[1].PrimarAdr = inst->SlaveIf. ;
	//	inst->SlaveManager.Slave[2].PrimarAdr = PRIMAER_ADRESS_ABB_1;
	//
	//
	//	 4:  Konfiguratzion der Timeouts 				
// RV if (inst->SlaveManager.bSlaveInit = false)
	if (inst->SlaveManager.bSlaveInit == 0)
	{
		//	inst->SlaveManager.SLAVE_RESP_TIMEOUT 	= T#20s;
		for (inst->SlaveManager.aktual_Slave_num = 1; inst->SlaveManager.aktual_Slave_num == inst->SlaveManagerIf.num_of_Slave; inst->SlaveManager.aktual_Slave_num++)
		{
			inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer.PT = 5000;
		} 
		inst->SlaveManager.aktual_Slave_num = 1;
		inst->SlaveManPaketIf.PaketIf.addressingTyp 	=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].addressingTyp;
		inst->SlaveManPaketIf.PaketIf.PrimarAdr 		=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].PrimarAdr;
		inst->SlaveManPaketIf.PaketIf.SecundarAdr 		=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].SecundarAdr;
		inst->SlaveManager.bSlaveInit = true;
	}
	
	
	
	if (inst->SlaveManPaketIf.bNextSlave) 
	{
		inst->SlaveManPaketIf.bNextSlave		= false;
		inst->SlaveManager.aktual_Slave_num 	= inst->SlaveManager.aktual_Slave_num + 1;
		
		if (inst->SlaveManager.aktual_Slave_num > inst->SlaveManagerIf.num_of_Slave)
		{
			inst->SlaveManager.aktual_Slave_num = 1;
		}
	}
			
	
	switch (inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt)
	{
		case SLAVE_WAIT:
			if (inst->SlaveManagerIf.bEnable)
			{
				inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_REQ; /* Automatischer Start  */
			}
			break;
	
			
		case SLAVE_SWITCH :
			//SlaveManager.aktual_Slave_num = SlaveManager.aktual_Slave_num;
			break;				
		
		case SLAVE_REQ :
		/* Anfrage f�r den ersten Slave konfigurieren. */				
			//memcpy(&(MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[0]), &(SlaveManager.Slave[SlaveManager.aktual_Slave_num].PrimarAdr), sizeof(USINT));
			
			//inst->SlaveManagerIf.num_of_Slave;
			
			/* Durchschalten das Adressen abh�ngig vom Slavetyp */
			inst->SlaveManPaketIf.PaketIf.addressingTyp 	=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].addressingTyp;
			inst->SlaveManPaketIf.PaketIf.PrimarAdr 		=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].PrimarAdr;
			inst->SlaveManPaketIf.PaketIf.SecundarAdr 		=  inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].SecundarAdr;
			
			inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer.IN 		= true;
			inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer.PT = inst->SlaveManagerIf.SlaveIf[inst->SlaveManager.aktual_Slave_num].SendCycleTime;
			TON(&inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer);
				
			if (inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer.Q) 
			{
				inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer.IN 	= false;
				TON(&inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].SendeTimer);
			
				inst->SlaveManPaketIf.bSendRequest 											 	= true;
				inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt 	= SLAVE_RES;
			}
			break;
			
		case SLAVE_RES :
			/* Wurde der Empfangenen Responseframe des ersten Slaves ausgewertet so kann der n�chste Request versendet werden. */
				
			inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut.IN = true;
			inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut.PT = SLAVE_RESP_TIMEOUT;
			TON(&inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut);
				
			if (inst->SlaveManagerIf.bFrameEvaluated)
			{ 
				inst->SlaveManPaketIf.bSendRequest 	= false;
				inst->SlaveManagerIf.bFrameEvaluated = false;
			
				if (inst->SlaveManPaketIf.bNextPaketWellcome) 
				{  /* TODO Zusammenstellung von gesplitteten Rohdatenanfragen */
					inst->SlaveManPaketIf.bNextSlave = true;
					//inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_REQ; /* Den n�chsten Slave abarbeiten.   */
					inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_WAIT; /* Den n�chsten Slave abarbeiten + Enable ber�cksichtigen  */
				}
				else
				{
					inst->SlaveManPaketIf.bNextSlave = true;
					//inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_REQ; /* Den n�chsten Slave abarbeiten.   */
					inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_WAIT; /* Den n�chsten Slave abarbeiten + Enable ber�cksichtigen  */
				}	
								
				inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut.IN = false;
				TON(&inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut);
				inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].usiRetries = 0;
			}		
			else if  (inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut.Q) 
			{
				inst->SlaveManPaketIf.bSendRequest 	= false;	
			 	inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut.IN = false;
				TON(&inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].ResponseTimeOut);
								
				if (inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].usiRetries < REQUEST_RETRIES) 
				{
					inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].usiRetries = inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].usiRetries + 1;
					inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = SLAVE_REQ; /* Den n�chsten Slave abarbeiten.   */
				}
				else
				{
					inst->SlaveManager.Slave[inst->SlaveManager.aktual_Slave_num].Arbeitsschritt = ERROR_SLAVE_SEQUENZ;
				}
			}
			break;				

		case  ERROR_SLAVE_SEQUENZ :
			inst->SlaveManPaketIf.bNextSlave = true;
			
			break;
	
		default:
	
			break;
	}
	
	inst->SlaveManPaketIf.aktSlaveNr = inst->SlaveManager.aktual_Slave_num;
}