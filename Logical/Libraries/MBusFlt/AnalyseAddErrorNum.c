/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: AnalyseAddErrorNum.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Analyse the additional Error Number */
UDINT AnalyseAddErrorNum(UDINT AddErrorNumber, UDINT adrString)
{
	UINT strcpyState; 
	//STRING ErrorString[80];
	/* The Additional Error Number stand on the FrameCopy[6] Element */
	switch (AddErrorNumber)
	{
		case NUM_OF_INDEX_REC :
			strcpy(adrString, "Number of Index Records is inkorrekt");
			break;
		
		case STREAM_LEN:
			strcpy(adrString,   "Sream Len inkorrekt");
			break;
		
		case INDEX_NUM :
			strcpy(adrString,   "Index Number is inkorrekt");
			break;
		
		case PARA_PER_INDEX_REC_WRONG:
			strcpy(adrString,   "Parameter pro Index Record is Incorekt");
			break;
		
		case INDEX_LEN_TO_SMALL :
			strcpyState = strcpy(adrString,   "Index Length"); 
			break;
		
		case PARA_NUM_INDEX_REC_0:
			strcpyState = strcpy(adrString,   "Parameter Number is inkorrekt"); 
			break;
		
		case PARA_LEN_INDEX_REC_0:
			strcpyState = strcpy(adrString,   "Parameter Length is inkorrekt.");
			break;
		
		case INKORREKT_ADR_TYP:
			strcpyState = strcpy(adrString,   "The Adress Typ is inkorrekt");
			break;
		
		case INKOREKT_ADR:
			strcpyState = strcpy(adrString,   "The Adress is inkorrekt");
			break;
		
		case INKOREKT_BAUDRATE:
			strcpyState = strcpy(adrString,   "The Transfer Bausrate is inkorrekt");
			break;
		
		case INKORREKT_TIMEOUT:
			strcpyState = strcpy(adrString,   "The Timeout is inkorrekt"); 
			break; 
		
		case INKORREKT_APENDIX_FRAME:
			strcpyState = strcpy(adrString,   "The Apendix Frame Configuration is inkorrekt");
			break; 
		
		default:
			break;
	}	
	
	return adrString;
}