(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: ErrorOutput.typ
 * Autor: peterlechnes
 * Erstellt: 11. August 2014
 ********************************************************************
 * Datentypen der Library MBusFlt
 ********************************************************************)
(*1 Error Handling*)

TYPE
	ErrorBits_typ : 	STRUCT 
		KollissionDetection : BOOL;
		ReadError : BOOL;
		BusWorkload : BOOL;
		Baudrate : BOOL;
	END_STRUCT;
	Error_typ : 	STRUCT 
		ErrorNumberBasic : UDINT;
		ErrorNumberAdd : UDINT;
		ErrorStringBasic : STRING[80];
		ErrorStringApendix : STRING[80];
		ErrorBits : ErrorBits_typ;
	END_STRUCT;
END_TYPE
