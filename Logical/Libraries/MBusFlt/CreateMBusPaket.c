/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: CreateMBusPaket.c
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Create the MBus Request Paket */
void CreateMBusRequest(struct CreateMBusRequest* inst)
{
	/* Das Umkopieren auf die Struktur dient lediglich um das Programm besser lesen zu k�nnen + besseres debugging  			*/
	/* 1: Hauptteil / Header ################################################################################################# 	*/
	inst->MBusAnfrage.Hauptteil.PacketZahler 			= inst->Requestpar.PaketCounter;     /* wird beim Versenden des Frame erh�ht. */
	inst->MBusAnfrage.Hauptteil.IndexRecordCount 	 = INDEX_RECORD_COUNT_HEAD;
	inst->MBusAnfrage.Hauptteil.ProtokollTyp1		 = PROTOKOLL_TYP;
	inst->MBusAnfrage.Hauptteil.ProtokollTyp2		 = PROTOKOLL_TYP;
	
	memcpy(&(inst->output_data[1]), &(inst->MBusAnfrage.Hauptteil), sizeof(inst->MBusAnfrage.Hauptteil));
	
	/* 2: Index Record 0 #######################################################################################################*/
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.1: Einleitung   																										*/
	inst->MBusAnfrage.IndexRecord0.Einleitung.IndexRecordType 	= INDEX_REC_TYP;
	inst->MBusAnfrage.IndexRecord0.Einleitung.Counter 			= INDEX_REC_COUNT_RC1;
	inst->MBusAnfrage.IndexRecord0.Einleitung.LenParBlockLow 	= LEN_PAR_BLOCK_LOW;
	inst->MBusAnfrage.IndexRecord0.Einleitung.LenParBlockHigh 	= LEN_PAR_BLOCK_HIGH;
	
	memcpy(&(inst->output_data[5]), &(inst->MBusAnfrage.IndexRecord0.Einleitung), sizeof(inst->MBusAnfrage.IndexRecord0.Einleitung));
	
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.2. Konfigurationsparameter 0 - Adressierungsart    																	*/
	/* Einstellen der Adressierungsart Prim�r = Adresse ist 1 Byte , Sekund�r = Adressierung �ber die Seriennummer = 4 Byte  	*/
	inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt.Parameternummer 		= PAR_NUM_ADR_ART;
	inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt.Lange			 	= LEN_ADR_ART;
	
	/* Hier kann mann zwischen Primar und Sekund�r = Adressierung �ber die Serialnummer umschalten 								*/ 
	if (inst->Requestpar.Adressierungsart == SEKUNDAR_ADR)
	{
		inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt.Adressierungsart = SEKUNDAR_ADR; 
	}
	else if(inst->Requestpar.Adressierungsart ==PRIMAR_ADR)
	{
		inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt.Adressierungsart = PRIMAR_ADR;
	}
	else if (inst->Requestpar.Adressierungsart ==BROADCAST_ADR )
	{
		inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt.Adressierungsart = PRIMAR_ADR;
	}
	
	memcpy(&(inst->output_data[9]), &(inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt), sizeof(inst->MBusAnfrage.IndexRecord0.KonfigParAdrArt));
	
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.3. Konfigurationsparameter 1 - Adresse    																				*/
	/* �bergeben der Adresse Abh�ngeig vom eingestellten Adresstyp 																*/
	
	inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Parameternummer 	= PAR_NUM_ADR;
	inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Lange			 	= LEN_ADR;
	//inst->Requestpar.SlaveSeriennummer; 
	
	if (inst->Requestpar.Adressierungsart == SEKUNDAR_ADR)
	{
		memcpy(&(inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[0]), &(inst->Requestpar.SlaveSeriennummer), sizeof(UDINT));
	}
	if (inst->Requestpar.Adressierungsart ==  PRIMAR_ADR) 
	{
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[0]  = inst->Requestpar.PrimarAdress;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[1] 	= 0;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[2] 	= 0;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[3] 	= 0;
	}
	else if (inst->Requestpar.Adressierungsart == BROADCAST_ADR)
	{
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[0]	= PIMAER_ADRESS_BROADCAST;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[1] 	= 0;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[2] 	= 0;
		inst->MBusAnfrage.IndexRecord0.KonfigParAdresse.Adresse[3] 	= 0;
	}
	
	memcpy(&(inst->output_data[12]), &(inst->MBusAnfrage.IndexRecord0.KonfigParAdresse), sizeof(inst->MBusAnfrage.IndexRecord0.KonfigParAdresse));
	
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.4 �bergeben der Baudrate  																								*/
	inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Parameternummer  	= PAR_NUM_BAUDRATE;
	inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Lange  			= LEN_BAUDRATE;
	inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Baudrate			= inst->Requestpar.Baudrate;
	
	
	inst->output_data[18] = inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Parameternummer;
	inst->output_data[19] = inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Lange;
	
	memcpy(&(inst->output_data[20]), &(inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Baudrate), sizeof(UINT)); 
	//memcpy(&(inst->output_data[21]), (UDINT)(&inst->MBusAnfrage.IndexRecord0.KonfigParBaudrate.Baudrate + 1), sizeof(USINT));
	
//	inst->output_data[20] = 0x60;
//	inst->output_data[21] = 0x9;
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.5 Parametrierung des Timeout offset */
	/* Offset deswegen weil er zum default Timeout dazuadiert werden muss. */
	
	inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.Parameternummer = PAR_NUM_TIMEOUT;
	inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.Lange			= LEN_TIMEOUT;
	inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.TimeoutOffset   = DEFAULT_TIMEOUT;
	
	inst->output_data[22] = inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.Parameternummer;
	inst->output_data[23] = inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.Lange;
	inst->output_data[24] = inst->MBusAnfrage.IndexRecord0.KonfigParTimeOutOffset.TimeoutOffset;
	
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 2.6 Parametrierung des Zusatzframes   																					*/
	
	inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Parameternummer 	= PAR_NUM_ZUSATZ_FRM;
	inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Lange 				= LEN_KFG_ZUSATZ;
	//inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Zusatzframe		= NO_MORE_FRAME;   /* Keine initialisierungs Frames */
	inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Zusatzframe			= NEXT_FRAME;   /* Keine initialisierungs Frames */
	
	inst->output_data[25] = inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Parameternummer;
	inst->output_data[26] = inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Lange;
	inst->output_data[27] = inst->MBusAnfrage.IndexRecord0.KonfigParZusatzframe.Zusatzframe;
	
	/* ########################################### 3: Index Record 1 ########################################################## */
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 3.1 Einleitung 																											*/
	inst->MBusAnfrage.IndexRecord1.Einleitung.IndexRecordType 		= DATEN_M_BUS_SLAVE;
	inst->MBusAnfrage.IndexRecord1.Einleitung.Counter 				= LESE_ROHDATEN;  /* 0 => keine weiteren Daten */
	//	inst->MBusAnfrage.IndexRecord1.Einleitung.Counter 				= 1;
	inst->MBusAnfrage.IndexRecord1.Einleitung.LangeParameterdatenLow 	= 0;
	inst->MBusAnfrage.IndexRecord1.Einleitung.LangeParameterdatenHigh = 0;
	
	inst->output_data[28] = inst->MBusAnfrage.IndexRecord1.Einleitung.IndexRecordType;
	inst->output_data[29] = inst->MBusAnfrage.IndexRecord1.Einleitung.Counter;
	inst->output_data[30] = inst->MBusAnfrage.IndexRecord1.Einleitung.LangeParameterdatenLow;
	inst->output_data[31] = inst->MBusAnfrage.IndexRecord1.Einleitung.LangeParameterdatenHigh;
	
	
	/* ------------------------------------------------------------------------------------------------------------------------ */
	/* 3.2 Parameter die Abgefragt werden sollen. 																				*/
	/* 3.2.1 Auslesen des ersten Parameters 																					*/
	//	inst->MBusAnfrage.IndexRecord1.DataParameter[0].ParameterNumber = 0;
	//	inst->MBusAnfrage.IndexRecord1.DataParameter[0].DataIndex			   = 19;  // 5
	//	output_data[32]				= inst->MBusAnfrage.IndexRecord1.DataParameter[0].ParameterNumber;
	//	output_data[33]				= inst->MBusAnfrage.IndexRecord1.DataParameter[0].DataIndex;
	//	//////	
	//	//////	/* 3.2.2 Auslesen des zweiten  Parameters 																					*/
	//	inst->MBusAnfrage.IndexRecord1.DataParameter[1].ParameterNumber = 1;			   /* Das ist eine fortlaufende Nummer */
	//	inst->MBusAnfrage.IndexRecord1.DataParameter[1].DataIndex		= 20; 	      	//12			 /* Das ist die Adresse des Registers */
	//	//////	
	//	output_data[34]		   = inst->MBusAnfrage.IndexRecord1.DataParameter[1].ParameterNumber;
	//	output_data[35]		   = inst->MBusAnfrage.IndexRecord1.DataParameter[1].DataIndex;
	//	//////
	//	////	
	//	MBusHandle;	
}