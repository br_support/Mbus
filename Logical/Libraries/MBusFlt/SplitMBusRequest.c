/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: SplitPaket.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/
/*  --------------------------- Content ---------------------------- */
/*  1: Error Handling 												*/
/*  2: Einen Nutzdaten Frame auswerten. 							*/
/*  2.1 Auswertung einer Rohdatenanfrage 							*/
/*  3: Auswertung einer gezielten Parameterabfrage.   				*/
/*  4: Den empfangenen Frame quitieren. 							*/
/*  ---------------------------------------------------------------- */

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif



/* Split the MBus Paket */
void SplitMBusResponse(struct SplitMBusResponse* inst)
{
	if (inst->bFrameRecive == true) 
	{
		/*  Auswerten des Empfangenen M BUS Frames => dieser steht auf dem ARRAY FrameCopy  */
		
		/*  1: Error Handling 	*/
		if (inst->FrameCopy[2] > 0) 
		{
			memcpy(&inst->ErrorAnalyser_0.StatusField, &inst->FrameCopy[2], sizeof(inst->ErrorAnalyser_0.StatusField));
			memcpy(&inst->ErrorAnalyser_0.StatusFieldAddon, &inst->FrameCopy[6], sizeof(inst->ErrorAnalyser_0.StatusFieldAddon));
			ErrorAnalyser(&inst->ErrorAnalyser_0);
		}
		else 
		{
			inst->ErrorAnalyser_0.StatusField = 0; 
			inst->ErrorAnalyser_0.StatusFieldAddon = 0;
			ErrorAnalyser(&inst->ErrorAnalyser_0);
		}
		/*  2: Einen Nutzdaten Frame auswerten. 						*/
		/*  2.1 Auswertung einer Rohdatenanfrage 					*/
		//AnalyseCrudeDataResponse;

		
		/*  3: Auswertung einer gezielten Parameterabfrage.   						*/
		//AnalyseParameterResponse;
		
		/*  4: Die Auswertung ist abgeschlossen. Den empfangenen Frame quitieren.  	*/
		/*  Beim n�chsten empfangenen Frame wird die Auswertung wieder getrigert 	*/
		//bFrameRecive 	= false;
		inst->bFrameEvaluated = true;   /*  Um zu erkennen das die Neue Anfrage gesendet werden kann */
		
	}
	else 
	{
		inst->bFrameEvaluated = false;	
	}
	
		
		/* FrameCopy wird jetzt bei jedem neuem Frame gel�scht. */
//		if (bClearFrameCopy)   
//		{
//			memset(&(FrameCopy), 0, SIZEOF(FrameCopy));
//			bClearFrameCopy = false;
//		}
}
