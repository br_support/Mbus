/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: ErrorAnalyser.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

#define MASK_BIT_0 0x01;
#define MASK_BIT_1 0x02;
#define MASK_BIT_2 0x04;
#define MASK_BIT_3 0x08; 

/* Analyse and detect an Error */
void ErrorAnalyser(struct ErrorAnalyser* inst)
{	
	//inst->ErrorOutput.ErrorNumberAdd = FrameCopy[6];
	inst->ErrorOutput.ErrorNumberAdd = inst->StatusFieldAddon;
	
	if ((inst->StatusField & 0x000000FF) != 0) 
	{
		
		inst->ErrorOutput.ErrorBits.KollissionDetection  =  (BOOL) inst->StatusField & MASK_BIT_0;
		inst->ErrorOutput.ErrorBits.ReadError			 =  (BOOL) inst->StatusField & MASK_BIT_1;
		inst->ErrorOutput.ErrorBits.BusWorkload		  	 =  (BOOL) inst->StatusField & MASK_BIT_2;
		inst->ErrorOutput.ErrorBits.Baudrate			 =  (BOOL) inst->StatusField & MASK_BIT_3;
	
		memcpy(&(inst->ErrorOutput.ErrorNumberBasic), &(inst->StatusField), sizeof(inst->ErrorOutput.ErrorNumberBasic));
		
		switch (inst->ErrorOutput.ErrorNumberBasic)
		{
			case NO_SLAVE_RESPONSE:
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "No Slave Response");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix); 
				break;
				
			case NO_SLAVE_RES_SEKUNDAER :
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "No Slave Response after Sekund�r Adressing");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix); 
				break;
				
			case INKORREKT_TRANSFER_PARAMETER:
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "Inkorrekt Transfer Parameter");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix);
				break;
				
			case DATA_KOLISSION:
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "Data Kolission on Bus");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix); 
				break;
				
			case INKORREKT_MODUL_CRC:
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "Inkorrekt Modul CRC");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix);
				break;
				
			case INKORREKT_IO_MODUL_STREAM:
				strcpy(&inst->ErrorOutput.ErrorStringBasic, "Inkorrekt CPU Modul Stream");
				AnalyseAddErrorNum(inst->ErrorOutput.ErrorNumberAdd, (UDINT)&inst->ErrorOutput.ErrorStringApendix);
				break;
				
			default: 
				strcpy(&inst->ErrorOutput.ErrorStringBasic, " ");
				break;
			
		}
	

	}
	else
	{
		inst->ErrorOutput.ErrorNumberBasic 	= 0;
		inst->ErrorOutput.ErrorNumberAdd 	= 0;
		strcpy(&inst->ErrorOutput.ErrorStringBasic, " ");
		strcpy(&inst->ErrorOutput.ErrorStringApendix," ");
	}	
}