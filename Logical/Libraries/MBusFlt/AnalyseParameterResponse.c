/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: AnalyseParameterResponse.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Analyse a Parameter Response */
void AnalyseParameterResponse(struct AnalyseParameterResponse* inst)
{
	
//		if ErrorOutput[SlaveManager.aktual_Slave_num].ErrorNumberBasic = 0 {  // TODO only if its a Parameter Data Response
//
//		/*  1.0: Split the Header into Parts */
//		ParameterDataResponse[1].FrameNummer 		:= FrameCopy[1];
//			ParameterDataResponse[1].ErrorNumber 		:= FrameCopy[2];
//			ParameterDataResponse[1].ParameterCounter 	:= FrameCopy[3];
//			ParameterDataResponse[1].MBusAdressePrim	:= FrameCopy[4];
//
//			memcpy(ADR(ParameterDataResponse[1].Seriennummer), ADR(FrameCopy[5]), SIZEOF(ParameterDataResponse[1].Seriennummer));
//		//ParameterDataResponse.Seriennummer := DWordBCDtoDezimal(ParameterDataResponse.Seriennummer);
//		memcpy(ADR(ParameterDataResponse[1].VendorID), ADR(FrameCopy[9]), SIZEOF(ParameterDataResponse[1].VendorID));
//		//ParameterDataResponse.VendorID := WordBCDtoDezimal(ParameterDataResponse.VendorID);	
//		ParameterDataResponse[1].DataStruktTyp := FrameCopy[11];
//
//			/*  1.1: Split each Parameter */
//			ParameterDataResponse[1].DatenStrukturMbus.Medium	:= FrameCopy[12];
//			ParameterDataResponse[1].DatenStrukturMbus.Index	:= FrameCopy[13];
//			ParameterDataResponse[1].DatenStrukturMbus.DataLen	:= FrameCopy[14];
//			ParameterDataResponse[1].DatenStrukturMbus.DIF		:= FrameCopy[15];
//			ParameterDataResponse[1].DatenStrukturMbus.VIF		:= FrameCopy[16];
//			memcpy(ADR(ParameterDataResponse[1].DatenStrukturMbus.Zahlerwert[0]), ADR(FrameCopy[17]), SIZEOF(ParameterDataResponse[1].DatenStrukturMbus.Zahlerwert));
//
//		/*  1.2: Split each Parameter */
//		ParameterDataResponse[2].DatenStrukturMbus.Medium  	:= FrameCopy[25];
//			ParameterDataResponse[2].DatenStrukturMbus.Index	:= FrameCopy[26];
//			ParameterDataResponse[2].DatenStrukturMbus.DataLen 	:= FrameCopy[27];
//			ParameterDataResponse[2].DatenStrukturMbus.DIF 		:= FrameCopy[28];
//			ParameterDataResponse[2].DatenStrukturMbus.VIF		:= FrameCopy[29];
//			memcpy(ADR(ParameterDataResponse[2].DatenStrukturMbus.Zahlerwert[0]), ADR(FrameCopy[30]), SIZEOF(ParameterDataResponse[2].DatenStrukturMbus.Zahlerwert));
//
//		/*  1.3: Split each Parameter */
//		ParameterDataResponse[3].DatenStrukturMbus.Medium  	:= FrameCopy[38];
//			ParameterDataResponse[3].DatenStrukturMbus.Index	:= FrameCopy[39];
//			ParameterDataResponse[3].DatenStrukturMbus.DataLen 	:= FrameCopy[40];
//			ParameterDataResponse[3].DatenStrukturMbus.DIF 		:= FrameCopy[41];
//			ParameterDataResponse[3].DatenStrukturMbus.VIF		:= FrameCopy[42];
//			memcpy(ADR(ParameterDataResponse[3].DatenStrukturMbus.Zahlerwert[0]), ADR(FrameCopy[43]), SIZEOF(ParameterDataResponse[2].DatenStrukturMbus.Zahlerwert));
//
//		END_IF;
//	
//		memset(&(FrameCopy), 0, SIZEOF(FrameCopy));
//		bClearFrameCopy = false;
	
		
}