/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: PaketEndDetect.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Detect the End of a Function  */
plcbit PaketEndDetect(unsigned long adrPaket, unsigned long sizeofPaket, UDINT* udiCopyoffset)
{
	plcbit bEndDetect;
	USINT* pArrayElement;
	UDINT x; 
	
	pArrayElement = adrPaket;
	
	for (x = 0; x <= sizeofPaket - 1; x++ )
	{
		if (*pArrayElement == 0x1F) 
		{
			bEndDetect = true;
			// bNextPaketWellcome = true; // TODO sich um bNextPaketWellcome k�mmern. 
			
			udiCopyoffset = x * sizeof(USINT);
			break;
		}
	}
		
	return bEndDetect;
}