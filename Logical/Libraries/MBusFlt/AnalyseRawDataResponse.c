/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: NeuerFunktionsBlock.c
 * Autor: peterlechnes
 * Erstellt: 7. August 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

/* Analyse Rohdata */
void AnalyseRawDataResponse(struct AnalyseRawDataResponse* inst)
{
	  /*  Rohdatenanalyse  */
//	if (inst->ErrorNumberBasic == 0)    // todo RohdatenResponse der auf mehrere Pakete aufgeteilt ist. && (inst->bNextPaketWellcome == true)) 
//	{    // TODO only if its Crude Data Response
//		
//			CrudeDataResponse.FrameSeqeunzCounter	:=  FrameCopy[1];
//			CrudeDataResponse.Error					:=  FrameCopy[2]; // wird bereits im Error handling ausgewertet.
//			CrudeDataResponse.SlaveData[0]			:=  FrameCopy[3]; 
//		
//			/*  Die Nutzdaten sind Slave spezifisch und m�ssen hir ausgewertet werden. 	*/
//		
//			/*  Add the korrekt Funktion */
//		
//
//		
//			//bNextPaketWellcome := false;
//	}
//	else /*  Ich muss das neue Paket hinten dran stopeln */
//	{	
//		
//		memcpy(&(ReadData) + uiCopyoffset, &(FrameCopy[3]), MIN(SIZEOF(FrameCopy), SIZEOF(ReadData))); 
//		
//		memcpy(&(ReadData), &(FrameCopy[3]), MIN(sizeof(FrameCopy), sizeof(ReadData))); 
//		
//		//CopyRohdaten1;
//
//		CopyAbbF4Rohdaten;
//		
//	}
//	
//	if (SlaveTyp == ABB_F4) 
//	{
//		CopyAbbF4Rohdaten;
//	}
//	else if (SlaveTyp == LANDIS_UH50) 
//	{
//		CopyLandisUH55;
//	}
//	else 
//	{
//		
//	} 
//	ClearFrameCopy;
}

//			ACTION CopyAbbF4Rohdaten :
//	
//				//if ErrorOutput[SlaveManager.aktual_Slave_num].ErrorNumberBasic = 16#70  {    // TODO only if its Crude Data Response
//				if FrameCopy[2] = 16#70 OR FrameCopy[2] = 16#0 OR FrameCopy[2] = 16#50 { // ABB Slave im allgemeinen Fehlerzustand.
//				ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.PaketZahler 	:=  FrameCopy[1];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.ErrorNummer  	:=  FrameCopy[2];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.StartSequenz1  :=  FrameCopy[3];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.L_Feld_1  		:=  FrameCopy[4];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.L_Feld_2  		:=  FrameCopy[5];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.StartSequenz2  :=  FrameCopy[6];
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.C_Feld			:=  FrameCopy[7];
//	
//					if ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.C_Feld = RSP_UD {
//					strProtokollTyp := 'LangSteuersatz';
//					END_IF;
//		
//					ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.A_Feld_PrimarAdr	:=  FrameCopy[8];
//						ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.CI_Feld			:=  FrameCopy[9]; // Wird im Kapitel 2.8 behandelt. 
//		
//						if ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.CI_Feld = 16#72 {
//						// Modus 1 
//						ELSIF ABB_F4_Energie[SlaveManager.aktual_Slave_num].Header.CI_Feld = 16#76 {
//							// Modus 2 Grundlagen zum M Bus => Kapitel 2.8.2
//							END_IF;
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderDaten.Identifikationsnummer), ADR(FrameCopy[10]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderDaten));
//		
//							//Seriennummer :=  DWordBCDtoDezimal(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderDaten.Identifikationsnummer);
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Seriennummer :=  DWordBCDtoDezimal(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderDaten.Identifikationsnummer);
//		
//								/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//								memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[1]),  ADR(FrameCopy[22]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[1]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[1].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[1].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[1]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumulierteEnergie),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[1].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumulierteEnergie));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumulierteEnergie := swapUDINT(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumulierteEnergie);
//		
//								/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//								memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[2]),  ADR(FrameCopy[28]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[2]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[2].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[2].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[2]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenGes),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[2].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenGes));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenGes := swapUDINT(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenGes);
//		
//								/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//								memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[3]),  ADR(FrameCopy[34]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[3]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[3].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[3].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[3]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenBereich),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[3].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.AkkumuliertesVolumenBereich));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[4]),  ADR(FrameCopy[41]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[4]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[4].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[4].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[4]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Vorlauftemperatur),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[4].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Vorlauftemperatur));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[5]),  ADR(FrameCopy[45]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[5]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[5].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[5].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[5]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Ruecklauftemperatur),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[5].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Ruecklauftemperatur));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[6]),  ADR(FrameCopy[49]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[6]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[6].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[6].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[6]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Temperaturdifferenz),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[6].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Temperaturdifferenz));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[7]),  ADR(FrameCopy[53]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[7]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[7].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[7].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[7]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Betriebszeit),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[7].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Betriebszeit));
//		
//							// TODO: Die Zeit belegt mehr als 4 Byte . Die Parameterstrucktur 7 muss aufgeblasen werden. 
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[8]),  ADR(FrameCopy[65]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[8]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[8].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[8].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[8]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.MomentanerDurchfluss),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[8].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.MomentanerDurchfluss));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[9]),  ADR(FrameCopy[71]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[9]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[9].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[9].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[9]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.MomentaneLeistung),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[9].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.MomentaneLeistung));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[10]),  ADR(FrameCopy[77]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[10]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[10].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[10].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[10]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ZeitDatum),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[10].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ZeitDatum));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[11]),  ADR(FrameCopy[84]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[11]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[11].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[11].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[11]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ImpulsregisterInput1),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[11].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ImpuslregisterInput2));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[12]),  ADR(FrameCopy[92]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[12]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[12].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[12].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[12]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ImpuslregisterInput2),  ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[12].Value[0]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.ImpuslregisterInput2));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[13]),  ADR(FrameCopy[98]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[13]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[13].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[13].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[13]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Monatswert[0]),  ADR(FrameCopy[100]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Monatswert[0]));
//		
//							/*  ----------------------------------------------- Auswertung Parameter X --------------------------------------------------------------------------------------------*/
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[14]),  ADR(FrameCopy[110]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[14]));
//		
//							ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[14].VIF_Value :=  ABB_F4_Energie[SlaveManager.aktual_Slave_num].ParameterDaten[14].DIFE;
//								ABB_F4_Energie[SlaveManager.aktual_Slave_num].VIF_Information[14]();
//		
//							memcpy(ADR(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Monatswert[1]),  ADR(FrameCopy[112]), SIZEOF(ABB_F4_Energie[SlaveManager.aktual_Slave_num].AnwenderSchnittstelle.Monatswert[1]));
//		
//							END_IF;
//	
//							END_ACTION

//----------------------------------------------------------------------------------------------------------------------
//			ACTION CopyRohdaten1:
//				//memcpy(ADR(ContoD4_1), ADR(FrameCopy[3]), MIN(SIZEOF(FrameCopy), SIZEOF(ReadData))); 
//				// Copy Header 
//				memcpy(ADR(ContoD4_1), ADR(FrameCopy[3]), MIN((SIZEOF(FrameCopy) - 3), SIZEOF(ContoD4_1))); 
//			memcpy(ADR(ContoD4_1), ADR(FrameCopy[3]), 20); 
//	
//			// Copy Parametersatz 1 
//			memcpy(ADR(ContoD4_1.Parameter_1), ADR(FrameCopy[22]), 10); 
//			//	ContoD4_1.Parameter_1.DIF := SHR(ContoD4_1.Parameter_1.DIF, 4);
//			//	ContoD4_1.Parameter_1.DIFE := SHR(ContoD4_1.Parameter_1.DIFE, 4);
//			//	ContoD4_1.Parameter_1.VIFE := SHR(ContoD4_1.Parameter_1.VIFE, 4);
//	
//			// Copy Parametersatz 2 
//			memcpy(ADR(ContoD4_1.Parameter_2), ADR(FrameCopy[31]), 10); 
//			//	ContoD4_1.Parameter_1.DIF := SHR(ContoD4_1.Parameter_1.DIF, 4);
//			//	ContoD4_1.Parameter_1.DIFE := SHR(ContoD4_1.Parameter_1.DIFE, 4);
//			//	ContoD4_1.Parameter_1.VIFE := SHR(ContoD4_1.Parameter_1.VIFE, 4);
//	
//			// Copy Parametersatz 3 
//			memcpy(ADR(ContoD4_1.Parameter_3), ADR(FrameCopy[40]), 10); 
//	
//			// Copy Parametersatz 4
//			memcpy(ADR(ContoD4_1.Parameter_4), ADR(FrameCopy[48]), 10); 
//	
//			END_ACTION

		