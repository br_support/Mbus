(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusHandler
 * Datei: SlaveOrganisation.typ
 * Autor: peterlechnes
 * Erstellt: 18. Dezember 2012
 ********************************************************************
 * Lokale Datentypen des Programs MBusHandler
 ********************************************************************)

TYPE
	SlaveHandler_enum : 
		(
		SLAVE_WAIT,
		SLAVE_SWITCH,
		SLAVE_REQ := 10,
		SLAVE_RES,
		ERROR_SLAVE_SEQUENZ
		);
	SlaveManagerPaketIf_typ : 	STRUCT 
		bSendRequest : BOOL;
		bNextPaketWellcome : BOOL;
		bNextSlave : BOOL;
		aktSlaveNr : USINT;
		PaketIf : SlaveIf_typ;
	END_STRUCT;
	SlaveIf_typ : 	STRUCT 
		SendCycleTime : TIME;
		SlaveIdent : USINT;
		addressingTyp : USINT;
		SecundarAdr : UDINT;
		PrimarAdr : USINT;
	END_STRUCT;
	Slave_typ : 	STRUCT 
		SlaveIf : SlaveIf_typ;
		Arbeitsschritt : SlaveHandler_enum;
		usiRetries : USINT;
		ResponseTimeOut : TON;
		SendeTimer : TON;
	END_STRUCT;
	SlaveManagerIf_typ : 	STRUCT 
		SlaveIf : ARRAY[0..MAX_NUM_OF_SLAVE]OF SlaveIf_typ;
		bEnable : BOOL;
		bFrameEvaluated : BOOL;
		num_of_Slave : USINT := 1;
	END_STRUCT;
	SlaveManager_typ : 	STRUCT 
		bSlaveInit : USINT;
		aktual_Slave_num : USINT;
		num_of_Slave : USINT := 1;
		Slave : ARRAY[0..MAX_NUM_OF_SLAVE]OF Slave_typ;
	END_STRUCT;
END_TYPE
