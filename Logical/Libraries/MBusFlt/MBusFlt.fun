(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: MBusFlt.fun
 * Autor: peterlechnes
 * Erstellt: 30. Juli 2014
 ********************************************************************
 * Funktionen und Funktionsblöcke der Library MBusFlt
 ********************************************************************)

FUNCTION_BLOCK SplitMBusResponse (*Split the MBus Paket*)
	VAR_INPUT
		bFrameRecive : BOOL;
		bExecute : BOOL;
		FrameCopy : ARRAY[0..INDEX_OF_INPUTDATA] OF USINT;
	END_VAR
	VAR_OUTPUT
		bFrameEvaluated : BOOL;
	END_VAR
	VAR
		ErrorAnalyser_0 : ErrorAnalyser;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION PaketEndDetect : BOOL (*Detect the End of a Function *)
	VAR_INPUT
		adrPaket : UDINT;
		sizeofPaket : UDINT;
		udiCopyoffset : REFERENCE TO UDINT;
	END_VAR
END_FUNCTION

FUNCTION_BLOCK MBusSlaveManager (*Organisation Sequence between the Slaves*)
	VAR_INPUT
		SlaveManagerIf : SlaveManagerIf_typ;
	END_VAR
	VAR_OUTPUT
		SlaveManPaketIf : SlaveManagerPaketIf_typ;
	END_VAR
	VAR
		SlaveManager : SlaveManager_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK FltGenHandler (*Flat Gen Handler => take a look at AsFltGen Library*)
	VAR_INPUT
		bExecuteRequest : BOOL;
		pRxBytes : ARRAY[0..15] OF USINT;
		bFrameEvaluated : BOOL;
		output_data : ARRAY[0..INDEX_OF_OUTDATA] OF USINT;
	END_VAR
	VAR_OUTPUT
		pTxBytes : ARRAY[0..15] OF USINT;
		FrameCopy : ARRAY[0..INDEX_OF_INPUTDATA] OF USINT;
		PaketCounter : USINT;
		bFrameRecive : BOOL;
	END_VAR
	VAR
		R_Trig_0 : R_TRIG;
		bSendRequest : BOOL;
		WorkingState : UINT;
		fltRead_0 : fltRead;
		fltWrite_0 : fltWrite;
		input_data : ARRAY[0..INDEX_OF_INPUTDATA] OF USINT;
		cfg_var : UDINT;
		bcfg_var : ARRAY[0..31] OF BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK ErrorAnalyser (*Analyse and detect an Error*)
	VAR_INPUT
		StatusFieldAddon : UDINT;
		StatusField : UDINT;
	END_VAR
	VAR_OUTPUT
		ErrorOutput : Error_typ;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK CreateMBusRequest (*Create the MBus Request Paket*)
	VAR_INPUT
		adrReqestArray : UDINT;
		sizeofRequestArray : BOOL;
		Requestpar : FB_if_par_typ;
	END_VAR
	VAR_OUTPUT
		output_data : ARRAY[0..INDEX_OF_OUTDATA] OF USINT;
	END_VAR
	VAR
		MBusAnfrage : MBusAnfrage_typ;
		usiPaket : USINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AnalyseRawDataResponse (*Analyse Rohdata*)
	VAR_INPUT
		bExecute : BOOL;
		ErrorNumberBasic : UINT;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION_BLOCK AnalyseParameterResponse (*Analyse a Parameter Response*)
	VAR_INPUT
		newParam : BOOL;
		newParam1 : BOOL;
	END_VAR
END_FUNCTION_BLOCK

FUNCTION AnalyseAddErrorNum : UDINT (*Analyse the additional Error Number*)
	VAR_INPUT
		AddErrorNumber : UDINT;
		adrString : UDINT;
	END_VAR
END_FUNCTION
