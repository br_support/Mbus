/********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusFlt
 * Datei: FltGenHandler.c
 * Autor: peterlechnes
 * Erstellt: 31. Juli 2014
 ********************************************************************
 * Implementierung der Library MBusFlt
 ********************************************************************/

#include <bur/plctypes.h>
#ifdef __cplusplus
	extern "C"
	{
#endif

	#include "MBusFlt.h"

#ifdef __cplusplus
	};
#endif

#define BIT_0_SET_MASKE 2#00000001
#define BIT_1_SET_MASKE 2#00000010
#define BIT_2_SET_MASKE 2#00000100
#define BIT_3_SET_MASKE 2#00001000
#define BIT_4_SET_MASKE 2#00010000

USINT index = 0; 
/* Help Funcions */
UDINT setbit(UDINT, USINT); 
plcbit readbit(UDINT, USINT);
//UDINT buildCfgDWord (plcbit[10]);
UDINT buildCfgDWord ();
UDINT minimum(UDINT, UDINT);

/* split Function */

/* Flat Gen Handler => take a look at AsFltGen Library */
void FltGenHandler(struct FltGenHandler* inst)
{

	UDINT CopyLen;
	
	inst->cfg_var = buildCfgDWord();
	
	/* 1.1 Configure the Write Functionblock. */
	inst->fltWrite_0.enable 		= true;
	inst->fltWrite_0.cfg 			= inst->cfg_var;
	inst->fltWrite_0.pBuf 			= &inst->output_data[1];
	inst->fltWrite_0.bufLen 		= sizeof(inst->output_data)-1;   // L�nge des Outputframes
	inst->fltWrite_0.pTxBytes 		= &inst->pTxBytes[0];
	inst->fltWrite_0.txBytesLen 	= sizeof(inst->pTxBytes);
	inst->fltWrite_0.pSequ 			= &inst->pRxBytes[0];
	inst->fltWrite_0.readSequ 		= inst->fltRead_0.readSequ;  //
	//inst->pTxBytes[0] 			=  inst->fltRead_0.readSequ;
	
	/* 1.2 Configure the Read Functionblock. */
	inst->fltRead_0.enable 			= true;
	inst->fltRead_0.cfg 			= inst->cfg_var;
	inst->fltRead_0.pBuf 			= &inst->input_data[1];
	inst->fltRead_0.bufLen 			= sizeof(inst->input_data);
	inst->fltRead_0.pRxBytes 		= &(inst->pRxBytes);
	inst->fltRead_0.rxBytesLen 		= sizeof(inst->pRxBytes);	
	
	fltRead(&inst->fltRead_0);
			
	inst->R_Trig_0.CLK = inst->bExecuteRequest;
	
	R_TRIG(&inst->R_Trig_0);
	
	if (inst->R_Trig_0.Q) 
	{
		inst->bSendRequest = true; 
	}
	
	if (inst->fltRead_0.status == fltERR_FRAME_FINISHED)
	{
		//DebuggStatusPoints;    (* Datenpunkte f�r das Debuggign *)
		if (inst->fltRead_0.validBytes > 0 )
		{
			/* TODO IF fltRead_0.pendBytes <> 0 THEN !!!!   */
			/* Aktuell input_data und FrameCopy so gro� w�hlen das der Buffer nich voll wird ==> pedingBytes immer 0 */
			//CopyLen = MIN(MIN(inst->fltRead_0.validBytes, sizeof(inst->FrameCopy)),sizeof(inst->input_data));
			CopyLen = minimum(inst->fltRead_0.validBytes, INDEX_OF_INPUTDATA);
			memset(&inst->FrameCopy[0], 0, sizeof(inst->FrameCopy));
			memcpy(&inst->FrameCopy[1], &inst->input_data[1], CopyLen);
			inst->bFrameRecive = true;
		}	
		
		/* 3 Write Funktionsblocks */
		inst->fltWrite_0.readSequ = inst->fltRead_0.readSequ;
		inst->fltWrite_0.bufLen = 0;

		/* 4: Write Aufruf  */		
		fltWrite(&inst->fltWrite_0); 
		
	}		
	else if  ((inst->bSendRequest && (inst->fltRead_0.status != fltERR_FRAME_FINISHED)) && (inst->fltRead_0.status != ERR_FUB_BUSY) && (inst->fltRead_0.validBytes == 0))
	{
		/* 3 Write Funktionsblocks */
		inst->fltWrite_0.readSequ = inst->fltRead_0.readSequ;
		inst->fltWrite_0.bufLen   = sizeof(inst->output_data) -1;   // L�nge des Outputframes			
		/* 4: Write Aufruf  */		
		fltWrite(&inst->fltWrite_0); 
	
		if  (inst->fltWrite_0.status == ERR_OK)
		{
			inst->bSendRequest 	= false;
			inst->PaketCounter	= inst->PaketCounter + 1;
			
			if (inst->PaketCounter == 0)
			{
				inst->PaketCounter = 1; 
			}
		}
	}
	else
	{
		/* 3 Write Funktionsblocks */
		inst->fltWrite_0.readSequ = inst->fltRead_0.readSequ;
		inst->fltWrite_0.bufLen = 0;

		/* 4: Write Aufruf  */		
		fltWrite(&inst->fltWrite_0);
	}
	
	if (inst->bFrameEvaluated)
	{
		inst->bFrameRecive = false; 	
	}
	
}


UDINT setbit(UDINT udiValue, USINT usiBitNr)
{
	UDINT udiResult;
	UDINT udiMask;
	USINT index = 1; 
	/*  Create a Mask */
	udiMask = 1; 
	
	for (index = 1; index <= usiBitNr; index++) 
	{
		udiMask = udiMask  * 2;
	}
	
	udiResult = (udiMask | udiValue); 
	
	return udiResult; 

}

/* Read one Bit of a UDINT */
plcbit readbit(UDINT udiValue, USINT usiBitNr)
{
	BOOL bValue; 
	UDINT udiMask;
	USINT index = 1; 
	/* Create a Mask */
	udiMask = 1; 
	
	for (index = 1; index <= usiBitNr; index++) 
	{
		udiMask = udiMask  * 2;
	}
	
	bValue = (BOOL)(udiMask & udiValue); 
	
	return bValue;
}

//UDINT buildCfgDWord (plcbit bcfg_var[10])
UDINT buildCfgDWord ()
{
	plcbit bcfg_var[32];
	UDINT udiCfg;
	USINT index;
	udiCfg = 0; 
	
	for (index = 0; index <= 30; index++) 
	{
		bcfg_var[index] = 0;
	}
	
	/* 1.  Configuration the cfg Var for the Flt FB. */
	/* 1.1 Configure the cfg Var for the FltGenWrite */
	bcfg_var[0] = readbit(IO_CYC_PER_TASK_CYC, 0);
	bcfg_var[1] = readbit(IO_CYC_PER_TASK_CYC, 1);
	bcfg_var[2] = readbit(IO_CYC_PER_TASK_CYC, 2);
	bcfg_var[3] = readbit(IO_CYC_PER_TASK_CYC, 3);
	bcfg_var[4] = readbit(FORWARD_MTU_NUM, 0);
	bcfg_var[5] = readbit(FORWARD_MTU_NUM, 1);
	bcfg_var[6] = readbit(FORWARD_MTU_NUM, 2);
	bcfg_var[7] = readbit(TX_SEQ_MODE, 0);
	bcfg_var[8] = readbit(TX_SEQ_MODE, 1);
	
	/* 1.2 Cunfigure the cfg Var for the FltRead 	*/
	bcfg_var[16] = readbit(RECIVE_MODE, 0);			// 			MODE_STREAM 
	bcfg_var[17] = readbit(RECIVE_MODE ,1);  		//			MODE_FRAME
	bcfg_var[18] = readbit(RECIVE_MODE ,2);   		//	 		MODE_PACKET
	
	
	for (index = 0; index <= 30; index++) 
	{
		if (bcfg_var[index])
		{
			udiCfg =  setbit(udiCfg, index);
			bcfg_var[index] = 0;
		}
	}
	
	//udiCfg = 65705;
	return udiCfg;
}

UDINT minimum(UDINT Value1, UDINT Value2)
{
	UDINT minimumValue;
	
	if (Value1 < Value2)
	{
		minimumValue = Value1;
	}
	else
	{
		minimumValue = Value2;
	}
	return minimumValue;
}
