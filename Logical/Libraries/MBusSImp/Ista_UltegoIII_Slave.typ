(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Library: MBusSImp
 * Datei: Ista_UltegoIII_Slave.typ
 * Autor: peterlechnes
 * Erstellt: 12. August 2014
 ********************************************************************
 * Datentypen der Library MBusSImp
 ********************************************************************)

TYPE
	MBusAnwenderDaten_typ : 	STRUCT 
		Identifikationsnummer : UDINT;
		Hersteller : UINT;
		Version : USINT;
		Medium : USINT;
		Zugriffszahl : USINT;
		Status : USINT;
		Signatur : UINT;
	END_STRUCT;
	MBusHeader_typ : 	STRUCT 
		PaketZahler : USINT;
		ErrorNummer : USINT;
		StartSequenz1 : USINT;
		L_Feld_1 : USINT;
		L_Feld_2 : USINT;
		StartSequenz2 : USINT;
		C_Feld : USINT;
		A_Feld_PrimarAdr : USINT;
		CI_Feld : USINT;
		New_Member : USINT;
	END_STRUCT;
	Ista_UltegoIII_typ : 	STRUCT 
		Header : MBusHeader_typ;
		AnwenderDaten : MBusAnwenderDaten_typ;
		MDMconnecting : BOOL;
		Start_auto_auslesen : BOOL;
		Zaehler1Nummer : UDINT;
		Zaehler1EnergieWh : REAL;
		Zaehler1Volumenm3 : REAL;
		Zaehler1Volumenfluss : REAL;
		Zaehler1Vorlauf : UDINT;
		Zaehler1Ruecklauf : UDINT;
		Zaehler2Nummer : UDINT;
		Zaehler2EnergieWh : REAL;
		Zaehler2Volumenm3 : REAL;
		Zaehler2Volumenfluss : REAL;
		Zaehler2Vorlauf : UDINT;
		Zaehler2Ruecklauf : UDINT;
		Zaehler1Leistung : REAL;
		Zaehler2Leistung : REAL;
		Auslesen_Stunden : USINT;
		Auslesen_Sekunden : USINT;
		Auslesen_Minuten : USINT;
		Zaehler3Ruecklauf : UDINT;
		Zaehler3Vorlauf : UDINT;
		Zaehler3Leistung : REAL;
		Zaehler3Volumenfluss : REAL;
		Zaehler3Volumenm3 : REAL;
		DTWZ2 : DATE_AND_TIME;
		Zaehler3EnergieWh : REAL;
		DTWZ1 : DATE_AND_TIME;
	END_STRUCT;
END_TYPE
