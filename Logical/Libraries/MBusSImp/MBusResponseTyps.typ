(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Program: MBusHandler
 * File: MBusResponseTyps.typ
 * Author: peterlechnes
 * Created: October 17, 2012
 ********************************************************************
 * Local data types of program MBusHandler
 ********************************************************************)
(*1 Error Handling*)

TYPE
	ResErrorBits_typ : 	STRUCT 
		KollissionDetection : BOOL;
		ReadError : BOOL;
		BusWorkload : BOOL;
		Baudrate : BOOL;
	END_STRUCT;
	ResError_typ : 	STRUCT 
		ErrorNumberBasic : UDINT;
		ErrorNumberAdd : UDINT;
		ErrorStringBasic : WSTRING[80];
		ErrorStringApendix : WSTRING[80];
		ErrorBits : ResErrorBits_typ;
	END_STRUCT;
END_TYPE

(*2:  Crude Data Analyse Rohdaten Antwort*)

TYPE
	ResCrudeDataResponse_typ : 	STRUCT 
		FrameSeqeunzCounter : USINT;
		Error : USINT;
		SlaveData : ARRAY[0..99]OF USINT;
	END_STRUCT;
END_TYPE

(*3: Parameter Respose Data Analyse*)

TYPE
	ResDatenStrukturMbus_typ : 	STRUCT 
		Medium : USINT;
		Index : USINT;
		DataLen : USINT;
		DIF : USINT;
		VIF : USINT;
		Zahlerwert : ARRAY[0..3]OF USINT;
	END_STRUCT;
	ResParameterResponse_typ : 	STRUCT 
		FrameNummer : USINT;
		ErrorNumber : USINT;
		ParameterCounter : USINT;
		MBusAdressePrim : USINT;
		Seriennummer : UDINT;
		VendorID : UINT;
		DataStruktTyp : USINT;
		DatenStrukturMbus : ResDatenStrukturMbus_typ;
	END_STRUCT;
END_TYPE
