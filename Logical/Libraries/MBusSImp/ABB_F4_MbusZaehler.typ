(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Datei: ABB_F4_MbusZaehler.typ
 * Autor: peterlechnes
 * Erstellt: 19. Dezember 2013
 ********************************************************************
 * Globale Datentypen des Projekts 20130911_MBus
 ********************************************************************)

TYPE
	ABB_F4_typ : 	STRUCT 
		Header : ABBHeader_typ;
		VIF_Information : ARRAY[0..15]OF VIF_Info;
		ParameterDaten : ARRAY[0..15]OF DWParameter_typ;
		AnwenderDaten : AnwenderDaten_typ;
		AnwenderSchnittstelle : Parameteraufbereitung_typ;
	END_STRUCT;
	Parameteraufbereitung_typ : 	STRUCT 
		Seriennummer : UDINT;
		AkkumulierteEnergie : UDINT;
		AkkumuliertesVolumenGes : UDINT;
		AkkumuliertesVolumenBereich : UINT;
		Vorlauftemperatur : UINT;
		Ruecklauftemperatur : UINT;
		Temperaturdifferenz : UINT;
		Betriebszeit : UINT;
		MomentanerDurchfluss : UINT;
		MomentaneLeistung : UINT;
		ZeitDatum : UINT;
		ImpulsregisterInput1 : UINT;
		ImpuslregisterInput2 : UINT;
		Monatswert : ARRAY[0..2]OF Monatswert_typ;
		New_Member1 : USINT;
		New_Member : USINT;
	END_STRUCT;
	Monatswert_typ : 	STRUCT 
		SpeicherDatum : ARRAY[0..3]OF USINT;
		AkkumulierteEnergie : UINT;
		AkkumuliertesVolumenGes : UINT;
		AkkumuliertesVolumenBereich : UINT;
	END_STRUCT;
	WParameter_typ : 	STRUCT 
		DIF : USINT;
		DIFE : USINT;
		Value : ARRAY[0..1]OF USINT;
	END_STRUCT;
	DWParameter_typ : 	STRUCT 
		DIF : USINT;
		DIFE : USINT;
		Value : ARRAY[0..3]OF USINT;
	END_STRUCT;
	AnwenderDaten_typ : 	STRUCT 
		Identifikationsnummer : UDINT;
		Hersteller : UINT;
		Version : USINT;
		Medium : USINT;
		Zugriffszahl : USINT;
		Status : USINT;
		Signatur : UINT;
	END_STRUCT;
	ABBHeader_typ : 	STRUCT 
		PaketZahler : USINT;
		ErrorNummer : USINT;
		StartSequenz1 : USINT;
		L_Feld_1 : USINT;
		L_Feld_2 : USINT;
		StartSequenz2 : USINT;
		C_Feld : USINT;
		A_Feld_PrimarAdr : USINT;
		CI_Feld : USINT;
		New_Member : USINT;
	END_STRUCT;
END_TYPE
