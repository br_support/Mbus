(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * File: Global.typ
 * Author: peterlechnes
 * Created: October 04, 2012
 ********************************************************************
 * Global data types of project MBus
 ********************************************************************)

TYPE
	End_Sequenz_typ : 	STRUCT 
		DIF : USINT;
		V1 : USINT;
		V2 : USINT;
		V3 : USINT;
		V4 : USINT;
		V5 : USINT;
		CS : USINT;
		Stop : USINT;
	END_STRUCT;
	DIF_VIF_VIFE_V4 : 	STRUCT 
		DIF : USINT;
		VIF : USINT;
		VIFE : USINT;
		Value : ARRAY[0..4]OF USINT;
	END_STRUCT;
	DIF_VIF_VIFE_V1 : 	STRUCT 
		DIF : USINT;
		VIF : USINT;
		VIFE : USINT;
		Value : ARRAY[0..4]OF USINT;
	END_STRUCT;
	DIF_DIFE_DIFE_VIF_V6_type : 	STRUCT 
		DIF : USINT;
		DIFE_1 : USINT;
		DIFE_2 : USINT;
		VIFE : USINT;
		VALUE : ARRAY[0..4]OF USINT;
	END_STRUCT;
	DIF_DIFE_DIFE_VIF_V4_type : 	STRUCT 
		DIF : USINT;
		DIFE_1 : USINT;
		DIFE_2 : USINT;
		VIFE : USINT;
		VALUE : ARRAY[0..4]OF USINT;
	END_STRUCT;
	DIF_DIFE_VIF_V4_type : 	STRUCT 
		DIF : USINT;
		DIFE : USINT;
		VIFE : USINT;
		VALUE : ARRAY[0..4]OF USINT;
	END_STRUCT;
	DIF_DIFE_VIF_V6_type : 	STRUCT 
		DIF : USINT;
		DIFE : USINT;
		VIFE : USINT;
		VALUE : ARRAY[0..6]OF USINT;
	END_STRUCT;
	CONTO_D4_Slave_typ : 	STRUCT 
		Start_Byte_1 : USINT;
		FrameLen1 : USINT;
		FrameLen2 : USINT;
		Start_Byte_2 : USINT;
		RSP_UD : USINT;
		PrimaryAdr : USINT;
		Struckt : USINT;
		Ident_1 : USINT;
		Ident_2 : USINT;
		Ident_3 : USINT;
		Ident_4 : USINT;
		ManufactureCodeLow : USINT;
		ManufactureCodeHigh : USINT;
		DeviceVersion : USINT;
		Medium : USINT;
		AccessNumber : USINT;
		Status : USINT;
		Signature_High : USINT;
		Signature_Low : USINT;
		Parameter_1 : DIF_DIFE_VIF_V6_type;
		Parameter_2 : DIF_DIFE_VIF_V4_type;
		Parameter_3 : DIF_DIFE_DIFE_VIF_V6_type;
		Parameter_4 : DIF_DIFE_DIFE_VIF_V4_type;
		Parameter_5 : DIF_DIFE_VIF_V6_type;
		Parameter_6 : DIF_DIFE_VIF_V4_type;
		Parameter_7 : DIF_DIFE_DIFE_VIF_V6_type;
		Parameter_8 : DIF_DIFE_DIFE_VIF_V4_type;
		Parameter_9 : DIF_VIF_VIFE_V4;
		Parameter_10 : DIF_VIF_VIFE_V1;
		EndSequenz : End_Sequenz_typ;
	END_STRUCT;
END_TYPE
