(********************************************************************
 * COPYRIGHT -- Bernecker + Rainer
 ********************************************************************
 * Programm: MBusHandler
 * Datei: VIF_Konstanten.typ
 * Autor: peterlechnes
 * Erstellt: 23. Dezember 2013
 ********************************************************************
 * Lokale Datentypen des Programs MBusHandler
 ********************************************************************)

TYPE
	VIF_Info : 	STRUCT 
		VIF_Value : USINT;
		Vield_Information : STRING[80];
		Add_Information : STRING[80];
	END_STRUCT;
	Medium_enum : 
		( (*Medium Tabelle*)
		Aneres := 16#00,
		OEL := 16#01,
		ELEKTRIZITAT := 16#02,
		GAS := 16#03,
		WARME := 16#04,
		DAMPF := 16#05,
		WARMWASSER := 16#06,
		WASSER := 16#07,
		WARMEMENGENZAHLER := 16#08,
		DRUCKLUFT := 16#09,
		RES_0A := 16#0A,
		RES_0B := 16#0B,
		WARME_VORLAUF_TEMP := 16#0C,
		RES_0D := 16#0D,
		BUS_SYSTEM := 16#0E,
		UNBEKANNTES_MEDIUM := 16#0F
		);
	Status_enum : 
		( (*Statusfeld Kap. 7.3.1*)
		OK := 2#00,
		Busy := 2#01,
		Anwendungsfehler := 2#10,
		reserviert := 2#11
		);
	VIF_Code_enum : 
		( (*Prim�re VIF_Codes *)
		EnergieWh := 2#0000000, (*E000 0nnn Energie Einheit = 10^(nnn - 3)  Wh*)
		EnergieWs := 2#0001000, (*E000 1nnn Energie Einheit = 10^(nnn) J*)
		Volumen := 2#0010000, (*E001 0nnn Volumen Einheit = 10^(nnn - 6) m^3*)
		Masse := 2#0011000, (*E001 1nnn Volumen Einheit = 10^(nnn - 3) kg*)
		Laufzeit := 2#0100000, (*E010 00nn Zeit Einheit nn = 00 Sekunden, nn=01 Minuten, nn = 10 Stunden, nn 11 = Tage *)
		LeistungWatt := 2#0101000, (*E010 1nnn Leistung Einheit = 10^(nnn - 3)  W*)
		LeistungJperH := 2#0110000, (*E011 0nnn Leistung Einheit = 10^(3)  Ws/h*)
		VolumenFluss := 2#0111000, (*E001 0nnn Volumen Einheit = 10^(nnn - 6) m^3/h*)
		VolumenFlussExt_m3h := 2#1000000, (*E001 0nnn Volumen Einheit = 10^(nnn - 7) m^3/min*)
		VolumenFlussExt_m3min := 2#1000000, (*E001 0nnn Volumen Einheit = 10^(nnn - 9) m^3/s*)
		VolumenFlussExt_m3s := 2#1001000,
		Massenfluss := 2#1010000,
		Vorlauftemperatur := 2#1011000,
		Ruecklauftemperatur := 2#1011100,
		Temperaturdifferenz := 2#1100000,
		Aussentemperatur := 2#1100100,
		Druck := 2#1101000,
		Zeitpunkt := 2#1101100,
		EinheitenWarmeGezahlt := 2#1101110,
		res := 2#1101100,
		Mittelungszeit := 2#1101100
		);
END_TYPE
