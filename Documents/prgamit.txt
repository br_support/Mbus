
//=======================================================================
// Specialni doplnek pre mereni 2NP Vodi�ka Trinity
//067 0x04 DIF 32bit integer
//068 0x13 VIF Volume 10(-6+3)
//069 0xAB 0    69 0x14
//070 0x31 1    70 0x32
//071 0x00 2    71 0x00
//072 0x00 3    72 0x00   
//
// Identifikace mereniv vodomer v prvnim patre vodicka
Let Zm__Ld1.1 =  Zm__TRd[0, 11]==0x77//011 0x77  1 Manuf.
Let Zm__Ld1.2 =  Zm__TRd[0, 12]==0x04//012 0x04  0
Let Zm__Ld1.3 =  Zm__TRd[0, 13]==0x14//013 0x14  0 Version
Let Zm__Ld1.4 =  Zm__TRd[0, 14]==0x07//014 0x07  0 Device type
//Let Zm__Ld1.5 =  Zm__TRd[0, 15]==0x30//015 0x30  0 Acces NO
//Let Zm__Ld1.6 =  Zm__TRd[0, 16]==0x10//016 0x10  0 Status
//Let Zm__Ld1.7 =  Zm__TRd[0, 17]==0x00//017 0x00  1 Signature
// Fabrication number
//Let Zm__Ld1.8 =  Zm__TRd[0, 21]==0x87//021 0x87  3 Fn
//Let Zm__Ld1.9 =  Zm__TRd[0, 22]==0x85//022 0x85  2 FN
//Let Zm__Ld1.10=  Zm__TRd[0, 23]==0x00//023 0x00  1 Fn
//Let Zm__Ld1.11=  Zm__TRd[0, 24]==0x11//024 0x11  0 Fn
//
Let Zm__Ld1.0 = Zm__Ld1.1 and Zm__Ld1.2 and Zm__Ld1.3 and Zm__Ld1.4 //and Zm__Ld1.5 and Zm__Ld1.6 and Zm__Ld1.7
//Let Zm__Ld1.0 = Zm__Ld1.8 and Zm__Ld1.9 and Zm__Ld1.10 and Zm__Ld1.11
if Zm__Ld1.0
    Let Zm__1NPCt = Zm__1NPCt +1
    Let Zm__Bf8[0, 0] = Zm__TRd[0, 69]
    Let Zm__Bf8[0, 1] = Zm__TRd[0, 70]
    Let Zm__Bf8[0, 2] = Zm__TRd[0, 71]
    Let Zm__Bf8[0, 3] = Zm__TRd[0, 72]
//StrParse Zm__Bf8, 0, NONE, "", 6, 4, "", Zm__RdFl, Zm__L9.8, -1.000
StrParse Zm__Bf8, 0, NONE, "", 2, 4, "", Zm__RdLg, Zm__L9.9, -1.000
    Let Zm__RdFl = float(Zm__RdLg)
    Let Zm__VFInx =  2
    Let Zm__VFexp = -3
    Let Zm__VFexpF= POW(10,Zm__VFexp)
    Let Zm__HodF  [Zm__PriTbInx,Zm__VFInx]=(Zm__RdFl * Zm__VFexpF)
    Let Zm__HodL  [Zm__PriTbInx,Zm__VFInx]= Zm__RdLg //long(Zm__RdLg * Zm__VIFRetMu)
    Let Zm__HodCt [Zm__PriTbInx,Zm__VFInx]= Zm__HodCt[Zm__PriTbInx,Zm__VFInx]+1
    Let Zm__HodExp[Zm__PriTbInx,Zm__VFInx]= Zm__VFexp
endif
//======================================================================= 